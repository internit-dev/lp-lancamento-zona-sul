<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.1.1">
    <title>Imóveis na Zona Sul Rio de Janeiro para comprar ou investir Apartamentos de 2, 3 e 4 Quartos.</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/carousel/">

    <!-- Bootstrap core CSS -->
    <link href="assets/dist/css/bootstrap.min.css" rel="stylesheet">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    
    <!-- STYLE -->
    <!-- Owl Stylesheets -->
    <link rel="stylesheet" href="style/lightbox/css/lightbox.min.css">
    <link rel="stylesheet" href="assets/owlcarousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/owlcarousel/assets/owl.theme.default.min.css">

    <link rel="stylesheet" href="style/owlslider/owl.carousel.min.css" />
	<link rel='stylesheet' href='style/style.css' media='all' />
	<link rel='stylesheet' href='style/mobile.css' media='all' />

    <!-- javascript -->
    <!-- <script src="../assets/vendors/jquery.min.js"></script> -->
    <!-- <script src="../assets/owlcarousel/owl.carousel.js"></script> -->

	
<!-- Event snippet for LP Zona Sul - Conversões conversion page -->
<script>
  gtag('event', 'conversion', {'send_to': 'AW-638188706/kPpZCPrGyI0CEKL5p7AC'});
</script>	
	

    <!-- FONT -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">

</head>
<body>

<div class="header" id="header">
  <div class="container">
    
    <div class="row">
      <div class="col-md-12">
        <h1>Morar na Zona Sul.</h1>
        <h2>As melhores opções para morar ou investir em imóveis.</h2>
      </div>
    </div><!--ROW-->

    <div class="boxinfo">
      <div class="row">
        <div class="col-md-6">
          
          <div class="escolha">
            <h1>Escolha onde quer morar:</h1>
            <a class="scroll" href="#botafogo">Botafogo</a>
            <a class="scroll" href="#lagoa">Lagoa</a>
            <a class="scroll" href="#gavea">Gávea</a>
          </div>

        </div>
        <div class="col-md-6">
              
          <div class="contato" id="contato">
            <div class="container">
                <div class="form">
                    <h1>Quero mais Informações</h1>

<?php

if ($_SERVER['REQUEST_METHOD'] === 'POST') {	
	
if (isset($_POST['nome'])) {	
	$vUrl = $_POST['vUrl'];
$nome     = $_POST['nome'];
$email    = $_POST['email'];
$tel      = $_POST['telefone'];
$emp      = $_POST['empreendimento'];
$msg	  = $_POST['mensagem'];
$vdt = date("d/m/Y");


$request = new HttpRequest();
$request->setUrl('http://crm.anapro.com.br/webcrm/webapi/integracao/v2/CadastrarProspect');
$request->setMethod(HTTP_METH_POST);
$request->setHeaders(array(
'cache-control' => 'no-cache',
'content-type'=> 'application/x-www-form-urlencoded'
));
$request->setContentType('application/x-www-form-urlencoded');
$request->setPostFields(array(
'Key' => 'zb3W3IC4wqI1',
'CampanhaKey'=> 'pNPFfenz6_Q1',
'ProdutoKey'=> 'kaLBBL4fpgY1',
'CanalKey'=> '32j6KY46Q_c1',
'Midia'=> 'Teste',
'PessoaNome'=> 'Fabrício Oficina',
'PessoaEmail'=> 'fabricio@oficina.com.br',
'Observacoes'=> 'Teste Oficina',
'KeyIntegradora'=> '3FE49887-F31C-4E33-88AE-08047FE8ABDF',
'KeyAgencia'=> 'c91ef847-c457-4086-8b51-ca4ca73e39f8',
'PessoaTelefones[0].DDD'=> '21',
'PessoaTelefones[0].Numero'=> '22445566',
));
try{
 $response = $request->send();
echo $response->getBody();
}catch (HttpException $ex){
 echo $ex;
}





$massunt = "Oficina Leads - Morar no ".$emp;

$mensagemHTML = "<br />
							 <strong>Dados do Contato solicitado através da landing page - Morar na Zona Sul</strong> <br /><br />
							 Empreendimento: $emp  \n<br />
							 Nome: $nome \n<br />
							 Email: $email  \n<br />
							 Telefone: $tel  \n<br />
							 Mensagem: $msg  \n<br /><br />
							 <br />Page URL: $vUrl \n";
	
	
$emaildestinatario = "leonardo@oficinainterativa.com";
 // $emaildestinatario = "fabricio.silva@oficinainterativa.com.br";


require_once("PHPMailer/class.phpmailer.php");

define('GUSER', 'performance.envios@gmail.com');	// <-- Insira aqui o seu GMail
define('GPWD', 'p3rf0rm@nc3123');		// <-- Insira aqui a senha do seu GMail	 

function smtpmailer($para, $de, $de_nome, $assunto, $corpo) { 
	global $error;
	$mail = new PHPMailer();
	$mail->IsSMTP();		// Ativar SMTP
	$mail->SMTPDebug = 0;		// Debugar: 1 = erros e mensagens, 2 = mensagens apenas
	$mail->SMTPAuth = true;		// Autenticação ativada
	$mail->SMTPSecure = 'tls';	// SSL REQUERIDO pelo GMail
	$mail->Host = 'smtp.gmail.com';	// SMTP utilizado
	$mail->Port = 587;  		// A porta 587 deverá estar aberta em seu servidor
	$mail->Username = GUSER;
	$mail->Password = GPWD;
	$mail->SetFrom($de, $de_nome);
	$mail->CharSet = 'UTF-8';
	$mail->IsHTML(true);
	$mail->Subject = $assunto;
	$mail->Body = $corpo;
	$mail->AddAddress($para);
	$mail->addBCC('getemailpop@gmail.com', 'Integracao Oficina');
	if(!$mail->Send()) {
		$error = 'Mail error: '.$mail->ErrorInfo; 
		return false;
	} else {
		$error = 'Mensagem enviada!';
		return true;
	}
}


 if (smtpmailer($emaildestinatario, $email, $nome, $massunt, $mensagemHTML)) {
	 
	echo "<br /><p style='color: #ffffff;'><strong> Muito obrigado, em breve entraremos em contato.</strong></p>";
	 
 } else { 
   echo "<span style='font-size: 9px;color: red;'>A mensagem não pode ser enviada - $error</span>";
   }

 }
 
}

?>					
					
					
                </div>
            </div>
        </div><!-- CONTATO -->

        <br class="clear" />

        </div>
      </div><!-- ROW -->
    </div><!--BOX INFO -->

  </div><!--CONTAINER -->

</div><!--HEADER-->


<div class="boxcont" id="botafogo">
 
  <div class="container">
    
    <div class="row">
      <div class="col-md-12">
        <h1 class="titulo">
          <span></span><span></span><span></span><span></span>
          Botafogo
          <span></span><span></span><span></span><span></span>
        </h1>
      </div>
    </div><!--ROW-->

    <div class="fotoprincipal">
    <div class="row">
      <div class="col-md-12">
        <img class="foto" src="style/images/foto_botafogo_01.jpg" />
        <span>Jardim Botafogo - 2, 3 e 4 Quartos</span> 
      </div>
    </div><!--ROW-->
  </div>

    <div class="infopredio">
    <div class="row">
      <div class="col-md-5">
        <p>
          O condomínio com a maior estrutura de lazer de Botafogo, que conduz para o bairro um empreendimento com design contemporâneo, que possuirá entretenimentos individualizados, segurança, sustentabilidade e tecnologia.
          <br /><br />
          Exclusivos apartamentos de 2, 3 e 4 quartos, de 76m² até 200m², que encantam pela sua sofisticação e vistas únicas!
        </p>
        <div class="fichatec">
          <strong>FICHA TÉCNICA</strong>
          <br /><br />
          X unidades<br />
          X apartamentos de 2 quartos<br />
          X apartamentos de 3 quartos<br />
          X coberturas de 3 quartos
        </div>
        <a href="#header" class="queromaisinfo scroll">Quero mais informações</a>
      </div>

      <div class="col-md-7">

        <div class="foto01" style="background-image: url(style/images/foto_botafogo_02.jpg);" ></div>
        <div class="foto02" style="background-image: url(style/images/foto_botafogo_03.jpg);" ></div>
        <div class="foto03" style="background-image: url(style/images/foto_botafogo_04.jpg);" ></div>
      </div>


    </div><!--ROW-->
  </div><!--infopredio-->


  <div class="fotoprincipal princ2">
    <div class="row">
      <div class="col-md-12">
        <a href="#header" class="scroll">
          <img class="foto" src="style/images/img_info.jpg" />
        </a>
        <span>Lançamento Rua Conde de Irajá - Em Breve</span> 
      </div>
    </div><!--ROW-->
  </div>

  </div><!--CONTAINER -->

</div><!-- BOX CONT -->


<div class="boxcont boxcont_branco" id="lagoa">
 
  <div class="container">
    
    <div class="row">
      <div class="col-md-12">
        <h1 class="titulo">
          <span></span><span></span><span></span><span></span>
          Lagoa
          <span></span><span></span><span></span><span></span>
        </h1>
      </div>
    </div><!--ROW-->

    <div class="fotoprincipal">
    <div class="row">
      <div class="col-md-12">
        <img class="foto" src="style/images/foto_lagoa_01.jpg" />
        <span>Borges 3647</span> 
      </div>
    </div><!--ROW-->
  </div>

    <div class="infopredio">
    <div class="row">
      <div class="col-md-5">
        <p>
          Borges 3647 Lagoa traz para você luxo e elegância em um dos bairros mais nobres do Rio de Janeiro. Borges 3647 Performance exibe sofisticação com toda originalidade localizado na Lagoa.
          <br /><br />
          Um dos bairros mas exclusivos da Zona Sul do Rio de Janeiro onde viver bem é viver com tranquilidade e comodidade no seu dia-a-dia com a natureza exuberante que cerca o Borges 3647 Lagoa.
        </p>
        <div class="fichatec">
          <strong>FICHA TÉCNICA</strong>
          <br /><br />
          X unidades<br />
          X apartamentos de 2 quartos<br />
          X apartamentos de 3 quartos<br />
          X coberturas de 3 quartos
        </div>
        <a href="#header" class="queromaisinfo scroll">Quero mais informações</a>
      </div>

      <div class="col-md-7">

        <div class="foto01" style="background-image: url(style/images/foto_lagoa_02.jpg);" ></div>
        <div class="foto02" style="background-image: url(style/images/foto_lagoa_03.jpg);" ></div>
        <div class="foto03" style="background-image: url(style/images/foto_lagoa_04.jpg);" ></div>
      </div>


    </div><!--ROW-->
  </div><!--infopredio-->

  </div><!--CONTAINER -->

</div><!-- BOX CONT -->



<div class="boxcont" id="gavea">
 
  <div class="container">
    
    <div class="row">
      <div class="col-md-12">
        <h1 class="titulo">
          <span></span><span></span><span></span><span></span>
          Gávea
          <span></span><span></span><span></span><span></span>
        </h1>
      </div>
    </div><!--ROW-->

    <div class="fotoprincipal">
    <div class="row">
      <div class="col-md-12">
        <img class="foto" src="style/images/foto_gavea_01.jpg" />
        <span>Contemporâneo Gávea</span> 
      </div>
    </div><!--ROW-->
  </div>

    <div class="infopredio">
    <div class="row">
      <div class="col-md-5">
        <p>
          O condomínio com a maior estrutura de lazer de Botafogo, que conduz para o bairro um empreendimento com design contemporâneo, que possuirá entretenimentos individualizados, segurança, sustentabilidade e tecnologia.
          <br /><br />
          Exclusivos apartamentos de 2, 3 e 4 quartos, de 76m² até 200m², que encantam pela sua sofisticação e vistas únicas!
        </p>
        <div class="fichatec">
          <strong>FICHA TÉCNICA</strong>
          <br /><br />
          X unidades<br />
          X apartamentos de 2 quartos<br />
          X apartamentos de 3 quartos<br />
          X coberturas de 3 quartos
        </div>
        <a href="#header" class="queromaisinfo scroll">Quero mais informações</a>
      </div>

      <div class="col-md-7">

        <div class="foto01" style="background-image: url(style/images/foto_gavea_02.jpg);" ></div>
        <div class="foto02" style="background-image: url(style/images/foto_gavea_03.jpg);" ></div>
        <div class="foto03" style="background-image: url(style/images/foto_gavea_04.jpg);" ></div>
      </div>


    </div><!--ROW-->
  </div><!--infopredio-->


  </div><!--CONTAINER -->

</div><!-- BOX CONT -->


 <div class="footer">
        <div class="container">
          <div class="row">
              <div class="col-md-12">
                © Copyright 2021 by Performance. All Rights Reserved.
              </div>
          </div><!--ROW-->
        </div><!-- CONTAINER -->
    </div><!-- LAZER -->

    



<script src="style/lightbox/js/lightbox-plus-jquery.min.js"></script>

<!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script> -->

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<script>window.jQuery || document.write('<script src="assets/js/vendor/jquery.slim.min.js"><\/script>')</script>
<script src="assets/dist/js/bootstrap.bundle.min.js"></script>



<!-- functions -->
        <script>
            $(document).ready(function() {
              $('.owl-carousel').owlCarousel({
                loop: true,
                margin: 10,
                responsiveClass: true,
                responsive: {
                  0: {
                    items: 1,
                    nav: true
                  },
                  600: {
                    items: 1,
                    nav: false
                  },
                  1000: {
                    items: 3,
                    nav: true,
                    loop: false,
                    margin: 20
                  }
                }
              })
            })
          </script>

<script src="style/js/functions.js"></script>

<script src="style/owlslider/owl.carousel.min.js"></script>

<script>
              $('.playvideo').click(function(){
                $(".playvideo").hide();
                $(".pausevideo").show();
              });
              
              $('.pausevideo').click(function(){
                $(".playvideo").show();
                $(".pausevideo").hide();
              });
            </script>


</body>
</html>
