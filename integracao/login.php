<?php
session_start();
if( isset($_POST['botao_logar']) ):

    $usuario = isset($_POST['user']) ? trim(strip_tags($_POST['user'])) : FALSE;
    $senha = isset($_POST['senha']) ? trim(strip_tags($_POST['senha'])) : FALSE;

    if( $usuario != FALSE && $senha != FALSE ) {

        if( $usuario == 'admin' && $senha == 'IN@abc' ) {

            $_SESSION['login'] = 'logado';
            header("location:index.php");

        } else{
            $mensagem = 'Informações Invalidas!';
        }

    }

endif;

?>




<!doctype html>
<html class="no-js" lang="pt_br">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Integração</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <link rel="shortcut icon" type="image/png" href="assets/images/icon/favicon.ico"> -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
    <link rel="stylesheet" href="assets/css/metisMenu.css">
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/slicknav.min.css">
    <!-- amchart css -->
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
    <!-- others css -->
    <link rel="stylesheet" href="assets/css/typography.css">
    <link rel="stylesheet" href="assets/css/default-css.css">
    <link rel="stylesheet" href="assets/css/styles.css">
    <link rel="stylesheet" href="assets/css/responsive.css">
    <link rel="stylesheet" href="assets/css/transition.css">
    <link rel="stylesheet" href="assets/css/dropdown.min.css">
    <link rel="stylesheet" href="assets/css/semantic.min.css">
    <!-- modernizr css -->
    <script src="assets/js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>

<!-- preloader area start -->
<div id="preloader">
    <div class="loader"></div>
</div>
<!-- preloader area end -->



<!-- #########################################################################################3 -->
<!-- #########################################################################################3 -->



<!-- Inicio area de logar -->
<div class="login-area login-s2">
    <div class="container">
        <div class="login-box ptb--100">


            <form method="POST" action="">

                <div class="login-form-head">
                    <div class="logo">
                        <a href="index.php"><img src="assets/images/icon/logo.png" alt="logo"></a>
                    </div>
                    <h4>Login</h4>
                    <p>Olá, Bem Vindo ao Integração Internit!</p>
                </div>

                <div class="text-center text-danger">
                    <?php if(isset($mensagem)){ echo $mensagem; } ?>
                </div>

                <div class="login-form-body">

                    <div class="form-gp">
                        <label >Usuário</label>
                        <input type="text" name="user" required>
                        <i class="ti-user"></i>
                        <div class="text-danger"></div>
                    </div>

                    <div class="form-gp">
                        <label >Senha</label>
                        <input type="password" name="senha" required>
                        <i class="ti-lock"></i>
                        <div class="text-danger"></div>
                    </div>

                    <div class="row mb-4 rmber-area">

                    </div>

                    <div class="submit-btn-area">
                        <button type="submit" name="botao_logar" >Logar <i class="ti-arrow-right"></i></button>
                    </div>

                </div>
            </form>

        </div>
    </div>
</div>
<!-- Inicio area de logar -->













<!-- #########################################################################################3 -->
<!-- #########################################################################################3 -->




<!-- jquery latest version -->
<script src="assets/js/vendor/jquery-2.2.4.min.js"></script>
<!-- bootstrap 4 js -->
<script src="assets/js/popper.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/owl.carousel.min.js"></script>
<script src="assets/js/metisMenu.min.js"></script>
<script src="assets/js/jquery.slimscroll.min.js"></script>
<script src="assets/js/jquery.slicknav.min.js"></script>
<script src="assets/js/transition.js"></script>
<script src="assets/js/dropdown.min.js"></script>
<script src="assets/js/semantic.min.js"></script>

<!-- start chart js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
<!-- start highcharts js -->
<script src="https://code.highcharts.com/highcharts.js"></script>
<!-- start zingchart js -->
<script src="https://cdn.zingchart.com/zingchart.min.js"></script>
<script>
    zingchart.MODULESDIR = "https://cdn.zingchart.com/modules/";
    ZC.LICENSE = ["569d52cefae586f634c54f86dc99e6a9", "ee6b7db5b51705a13dc2339db3edaf6d"];
</script>
<!-- all line chart activation -->
<script src="assets/js/line-chart.js"></script>
<!-- all pie chart -->
<script src="assets/js/pie-chart.js"></script>
<!-- others plugins -->
<script src="assets/js/plugins.js"></script>
<script src="assets/js/scripts.js"></script>
<script>

    $('.ui.dropdown').dropdown();

    $('.ui.selection.dropdown').dropdown({ clearable: true });

    $('.ui.inline.dropdown').dropdown({ clearable: true, placeholder: 'any' });

    $('.ui.normal.dropdown').dropdown({ maxSelections: 3 });

    $('#select_destaque').dropdown({ clearable: false });

</script>

</body>

</html>
