<?php if($portaldocorretorConfiguracoes['status'] == true): ?>

<script>

    const token = '<?= $portaldocorretorConfiguracoes["token"] ?>';

/*
 let formularioDados = {
        'formId': formId,
        'formNome': formNome,
        'nome': nome,
        'email': email,
        'telefoneCompleto': telefoneCompleto,
        'telefoneSemDDD': telefoneSemDDD,
        'ddd': ddd,
        'mensagem': mensagem,
        'termos': termos,
      };
*/

    /**
     * Função Padrão que Realizará a Integração da Portal do Corretor
     * @param formularioDados
     * @param dispositivo
     * @param parametrosUrl
     */
    function portaldocorretorAplicarIntegracao(formularioDados, dispositivo, parametrosUrl)
    {
        //alert('Dentro Aplica Integração Portal Corretor');
        let resposta = {};


        let fieldSelectors = {
            "name": formularioDados['nome'],
            "email": formularioDados['email'],
            "phone": formularioDados['telefoneCompleto']
        };

        let response = integratePortal(formularioDados['formId'], fieldSelectors, formularioDados['formNome']);

        response.always(function(data) {

            //console.log(data)

            let mensagem = data['message'];
            let status = data['statusText'];


                resposta = {
                    'status': false,
                    'mensagem': 'Erro Indefinido, Verificar Post para Mais Informações',
                };

            if(status == 'error') {
                resposta = {
                    'status': false,
                    'mensagem': 'Verificar Erro De Cors e ID',
                };
            }

            if(mensagem == 'Product not found'){
                resposta = {
                    'status': false,
                    'mensagem': 'Produto não encontrado - Verificar o ID',
                };
            }

            if(mensagem == 'Lead created'){
                resposta = {
                    'status': true,
                    'mensagem': 'Leed Criado Com Sucesso',
                };
            }

        })

        return resposta;

    }

/**
 * Função principal para enviar lead ao portal do corretor
 * @param {string} formSelector seletor do formulário que será enviado
 * @param {object} fieldSelectors par (nome do atributo do lead, seletor do campo que possui o valor)
 * @param {string} group grupo do formulário pertencente à campanha
 * @param {function} successCallback função a ser executada caso o lead seja enviado com sucesso
 * @param {function} errorCallback função a ser executada caso haja falha no envio do lead
 * @param {function} alwaysCallback função a ser chamada no fim 
 */
function integratePortal(formSelector, fieldSelectors, group, successCallback = function(response) {}, errorCallback = function(err) {}, alwaysCallback = function() {})
{
    let lead = createLead(formSelector, fieldSelectors, group);

    return sendLead(lead, successCallback, errorCallback, alwaysCallback);
}

/**
 * Cria o lead a ser enviado para o portal do corretor.
 * @param {string} formSelector seletor do formulário que será enviado
 * @param {object} fieldSelectors par (nome do atributo do lead, seletor do campo que possui o valor)
 * @param {string} group grupo do formulário pertencente à campanha
 * @return {object} lead
 */
function createLead(formSelector, fieldSelectors, group) {

  var lead = fieldSelectors;
  lead["origem"] = document.location.host;
  lead["group"] = group + '-LP';
  lead["token"] = token;
  return lead;
}

/**
 * Faz a chamada ajax para enviar o lead ao portal do corretor
 * @param {string} url endereço da rota que receberá o lead no portal do corretor
 * @param {object} lead lead a ser salvo no portal do corretor
 * @param {function} successCallback função a ser executada caso o lead seja enviado com sucesso
 * @param {function} errorCallback função a ser executada caso haja falha no envio do lead
 * @param {function} alwaysCallback função a ser executada no fim da chamada ajax 
 *    independentemente do resultado da chamada
 */
function sendLead( lead, successCallback, errorCallback, alwaysCallback)
{
  // alert('ALOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO')
  return jQuery.ajax({
    "type": "POST",
    "async": false,
    "url": 'https://volendam.descomplicavendas.com.br/api/leads/send',
    "data": JSON.stringify(lead),
  })/*
  .done(function(response) {
    successCallback(response);
  })
  .fail(function(err) {
    errorCallback(err);
  })
  .always(function() {
    alwaysCallback();
  }) */
}

/**
 * Obtém os valores dos campos do formulário.
 * @param {string} formSelector seletor do formulário a ser extraído
 * @param {object} fieldSelectors pares (nome do atributo usado no lead, seletor do campo dentro do form)
 */
function getFormValues(formSelector, fieldSelectors)
{
  var values = {};

  // atributos do objeto sem incluir os atributos do prototype
  var keys = Object.keys(fieldSelectors);

  keys.forEach(function(key) {
    let val = jQuery(formSelector + fieldSelectors[key]).val();
    values[key] = val != null ? val : "";
  });

  return values;
}


</script>


<?php endif; ?>