<?php if($fbpixelConfiguracoes['status'] == true): ?>

<!-- Facebook Pixel Code -->
<script>
    /**
     let formularioDados = {
        'formId': formId,
        'formNome': formNome,
        'nome': nome,
        'email': email,
        'telefoneCompleto': telefoneCompleto,
        'telefoneSemDDD': telefoneSemDDD,
        'ddd': ddd,
        'mensagem': mensagem,
        'termos': termos,
    };
     **/


    /**
     * Função Padrão que Realizará a Integração da Anapro
     * @param formularioDados
     * @param dispositivo
     * @param parametrosUrl
     */
    function fbpixelAplicarIntegracao(formularioDados, dispositivo, parametrosUrl)
    {

        //alert('FB Pixel Aplica Integração')

        let resposta = {};


        sendFbq(formularioDados['empreendimento']);

        resposta = {
            'status': true,
            'mensagem': 'Sem Resposta Padrão - Usar Plugin (FB Pixel Helper) para Validar',
        };

        return resposta;
    }


    /**
     * Envia eventos de fbq para um form de contato.
     * Dentro desta função, você pode fazer tantas chamadas a fbq quanto for necessário.
     */
    function sendFbq(formType){
        fbq('track', 'Lead', {
            'content_name': formType
        });
    }


! function(f, b, e, v, n, t, s) {
  if (f.fbq) return;
  n = f.fbq = function() {
    n.callMethod ?
      n.callMethod.apply(n, arguments) : n.queue.push(arguments)
  };
  if (!f._fbq) f._fbq = n;
  n.push = n;
  n.loaded = !0;
  n.version = '2.0';
  n.queue = [];
  t = b.createElement(e);
  t.async = !0;
  t.src = v;
  s = b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t, s)
}(window, document, 'script',
  'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '<?= $fbpixelConfiguracoes['id'] ?>');
fbq('track', 'PageView');

</script>

<noscript>
    <img height="1" width="1" alt="facebook" style="display:none"
    src="https://www.facebook.com/tr?id=<?= $fbpixelConfiguracoes['id'] ?>&ev=PageView&noscript=1" />
</noscript>





<?php endif; ?>