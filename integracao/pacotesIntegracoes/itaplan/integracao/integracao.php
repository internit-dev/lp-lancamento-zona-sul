<?php if($itaplanConfiguracoes['status'] == true): ?>

<script>
/*********************************************************************************************
 * ************************************* Parâmetros ******************************************
 * *******************************************************************************************
 */

const itaplanUsuario = '<?= $itaplanConfiguracoes["usuario"] ?>';
const itaplanSenha = '<?= $itaplanConfiguracoes["senha"] ?>';
/*********************************************************************************************
 * ************************************** Funções ********************************************
 * *******************************************************************************************
*/

/*
 let formularioDados = {
        'formId': formId,
        'formNome': formNome,
        'nome': nome,
        'email': email,
        'telefoneCompleto': telefoneCompleto,
        'telefoneSemDDD': telefoneSemDDD,
        'ddd': ddd,
        'mensagem': mensagem,
        'termos': termos,
      };
*/

/**
 * Função Padrão que Realizará a Integração da Anapro
 * @param formularioDados
 * @param dispositivo
 * @param parametrosUrl
 */
function itaplanAplicarIntegracao(formularioDados, dispositivo, parametrosUrl) {

    //alert('Dentro Aplica Integração Itaplan');
    let resposta = {};

    let response = iniciarIntegracaoItaplan(formularioDados['FormId'], itaplanUsuario, itaplanSenha);


    response.always(function(data){

        let status = data['status'];

        //console.log(data)

        resposta = {
            'status': true,
            'mensagem': 'Leed Enviado ',
        };

        if(status == 500){
            resposta = {
                'status': false,
                'mensagem': 'Erro 500, Possivelmente Id Incorreto',
            };
        }

    })



    return response;

}

/**
 * 
 * @param {*} usuario 
 * @param {*} senha 
 */
function obterToken(usuario, senha) {
  return new Promise((resolve) => {
    $.ajax({
      crossDomain: true,
      contentType: 'application/x-www-form-urlencoded',
      type: 'POST',
      url: 'https://itaplan.sigavi360.com.br/Sigavi/api/Acesso/Token',
      data: `username=${usuario}&password=${senha}&grant_type=password`,
      success: function (data) {
        resolve(data.access_token);
      },
      error: function (data) {
        alert('Erro na autenticação!');
        console.log(data);
      }
    });
  });
}

/**
 * 
 * @param {*} formSelector 
 * @param {*} usuario 
 * @param {*} senha 
 */
function iniciarIntegracaoItaplan(formSelector, usuario, senha) {

  let lead = objectForm($(formSelector).serializeArray());

  lead['Email'] = (lead['Email']) ? lead['Email'] : 'teste@internit.com.br';


  let promise = obterToken(usuario, senha);
  promise.then(token => {
      registrarLead(token, lead);
  })
  .catch(erro => {
    console.log(erro);
      console.log('Erro ao registrar o lead Itaplan');
  });
}

/**
 * 
 * @param {*} token 
 * @param {*} lead 
 */
function registrarLead(token, lead) {
  return $.ajax({
    url: 'https://itaplan.sigavi360.com.br/Sigavi/api/Leads/NovaLead',
    type: 'POST',
    dataType: 'JSON',
    contentType: 'application/json',
    data: JSON.stringify(lead),
    beforeSend: function (request) {
      request.setRequestHeader('Authorization', `Bearer ${token}`);
    },
    success: function (data) {
      if (data['Sucesso']) {
        // lead['SigaviLeadId'] = data['Id'];
        processarLead(lead);
      }
      else {
        let erros = 'Erro:\n';
        data.Erros.forEach(erro => {
          erros += erro + '\n';
        });
        //alert(erros);
          console.log('Erro ao registrar o lead Itaplan');
      }
    },
    error: function (data) {

        console.log('Erro ao registrar o lead Itaplan');
    }
  });
}

</script>

<?php endif; ?>