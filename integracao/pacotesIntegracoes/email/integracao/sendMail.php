<?php
header('Content-Type: application/json');

require '../../../../vendor/autoload.php';

$emailConfiguracoes = require_once '../configuracoes/configuracoes.php';


use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;


$formId = $_REQUEST['formId'];
$formNome = $_REQUEST['formNome'];
$nome = $_REQUEST['nome'];
$email = $_REQUEST['email'];
$telefoneCompleto = $_REQUEST['telefoneCompleto'];
$telefoneSemDDD = $_REQUEST['telefoneSemDDD'];
$ddd = $_REQUEST['ddd'];
$mensagem = $_REQUEST['mensagem'];
$termos = $_REQUEST['termos'];


//var_dump($_REQUEST);

//var_dump($emailConfiguracoes);


$var = true;

$emailsEnviados = '';

if( $emailConfiguracoes['status'] ):

    foreach ($emailConfiguracoes['sendTo'] as $emailEnviar):

        $mail = new PHPMailer;

        $mail->isSMTP();                      // Set mailer to use SMTP
        $mail->Host = $emailConfiguracoes['smtpConfig']['Host'];       // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;               // Enable SMTP authentication
        $mail->Username = $emailConfiguracoes['smtpConfig']['Username'];   // SMTP username
        $mail->Password = $emailConfiguracoes['smtpConfig']['Password'];   // SMTP password
        $mail->SMTPSecure = $emailConfiguracoes['smtpConfig']['SMTPSecure'];            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = $emailConfiguracoes['smtpConfig']['Port'];                    // TCP port to connect to

        // Sender info
        $mail->setFrom($emailConfiguracoes['smtpConfig']['FromEmail'], $emailConfiguracoes['smtpConfig']['FromName']);

        // Add a recipient
        $mail->addAddress($emailEnviar);

        // Set email format to HTML
        $mail->isHTML(true);

        // Mail subject
        $mail->Subject = 'Contato - '.$emailConfiguracoes['smtpConfig']['FromName'];


        $bodyContent = "<h1>Contato Realizado - ". $emailConfiguracoes['smtpConfig']['FromName'] ."</h1>";

        $bodyContent .= '<p>';
        $bodyContent .= '<b>Formulário Preenchido: </b>' . $formNome;
        $bodyContent .= '<br><b>Nome:</b> ' . $nome;
        $bodyContent .= '<br><b>Email:</b> ' . $email;
        $bodyContent .= '<br><b>Telefone:</b> ' . $ddd . ' ' . $telefoneSemDDD;
        if($mensagem != ''){
            $bodyContent .= '<br><b>Mensagem:</b> ' . $mensagem;
        }
        $bodyContent .= '</p>';

        $mail->Body = $bodyContent;





        if($mail->send()) {

            $emailsEnviados .= $emailEnviar .' , ';

        } else {


        }

    endforeach;

        $resposta = [
            'status' => false,
            'emailEnviados' => [],
        ];

        if(!is_null($emailsEnviados)){
            $resposta['status'] = true;
            $resposta['emailEnviados'] = $emailsEnviados;
        }

        echo json_encode($resposta);



endif;

