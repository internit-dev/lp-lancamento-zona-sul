<?php if($emailConfiguracoes['status'] == true): ?>


<script>
    /*
     let formularioDados = {
            'formId': formId,
            'formNome': formNome,
            'nome': nome,
            'email': email,
            'telefoneCompleto': telefoneCompleto,
            'telefoneSemDDD': telefoneSemDDD,
            'ddd': ddd,
            'mensagem': mensagem,
            'termos': termos,
          };
    */

    /**
     * Função Padrão que Realizará a Integração da Portal do Corretor
     * @param formularioDados
     * @param dispositivo
     * @param parametrosUrl
     */
    function emailAplicarIntegracao(formularioDados, dispositivo, parametrosUrl)
    {
        //alert('Dentro de Aplica Integracao Email');
        let resposta = {};

        let response = sendMail(formularioDados, 'alan@internit.com.br');

        response.always(function(data){

            resposta = {
                'status': data['status'],
                'mensagem': 'Emails Enviado Para: ' + data['emailEnviados'],
            };

        })

        return resposta;

    }

    function sendMail(formularioDados, emailVez){
        return $.ajax({
            async: false,
            type: 'POST',
            url: 'integracao/pacotesIntegracoes/email/integracao/sendMail.php',
            data: formularioDados
        });
    }


</script>

<?php endif; ?>