
    <h1> Dados Email: </h1>
    <br>

        <div class="row">

            <div class="form-check form-switch mb-3" style="margin-left: 20px;">
                <input class="form-check-input" type="checkbox" name="status" <?php if($emailConfiguracoes['status'] == true): ?>  checked <?php endif ?> >
                <label class="form-check-label">Status</label>
            </div>

            <h2> SMTP Config: </h2>

            <div class="row">

                <div class="mb-3 col-4">
                    <label class="form-label">Host</label>
                    <input type="text" name="Host" class="form-control" value="<?= $emailConfiguracoes['smtpConfig']['Host'] ?>" placeholder="">
                </div>

                <div class="mb-3 col-4">
                    <label class="form-label">Username</label>
                    <input type="text" name="Username" class="form-control" value="<?= $emailConfiguracoes['smtpConfig']['Username'] ?>" placeholder="">
                </div>

                <div class="mb-3 col-4">
                    <label class="form-label">Password</label>
                    <input type="password" name="Password" class="form-control" value="<?= $emailConfiguracoes['smtpConfig']['Password'] ?>" placeholder="">
                </div>

                <div class="mb-3 col-4">
                    <label class="form-label">Port</label>
                    <input type="number" name="Port" class="form-control" value="<?= $emailConfiguracoes['smtpConfig']['Port'] ?>" placeholder="Default 587">
                </div>

                <div class="mb-3 col-4">
                    <label class="form-label">SMTPSecure</label>
                    <select class="form-select" name="SMTPSecure" aria-label="Default select example">
                        <option selected>Selecione</option>
                        <option <?php if($emailConfiguracoes['smtpConfig']['SMTPSecure'] == 'ssl'): echo 'selected'; endif; ?> value="ssl">SSL</option>
                        <option <?php if($emailConfiguracoes['smtpConfig']['SMTPSecure'] == 'tls'): echo 'selected'; endif; ?> value="tls">TLS</option>
                    </select>
                </div>

                <div class="mb-3 col-4">
                    <label class="form-label">FromEmail</label>
                    <input type="text" name="FromEmail" class="form-control" value="<?= $emailConfiguracoes['smtpConfig']['FromEmail'] ?>" placeholder="">
                </div>

                <div class="mb-3 col-4">
                    <label class="form-label">FromName</label>
                    <input type="text" name="FromName" class="form-control" value="<?= $emailConfiguracoes['smtpConfig']['FromName'] ?>" placeholder="">
                </div>

                <h2> Enviar para Emails: </h2>


                    <?php $i = 1; foreach ($emailConfiguracoes['sendTo'] as $emails): ?>

                        <div class="mb-3 col-4">
                            <label class="form-label">Email <?= $i; ?>:</label>
                            <input type="email" name="sendTo-<?= $i; ?>" value="<?= $emails ?>" class="form-control">
                        </div>

                    <?php $i++; endforeach; ?>

                    <?php if ($i < 8): ?>
                        <?php for (; $i<=8; $i++): ?>

                            <div class="mb-3 col-4">
                                <label class="form-label">Email <?= $i; ?>:</label>
                                <input type="email" name="sendTo-<?= $i; ?>" value="" class="form-control">
                            </div>

                        <?php endfor; ?>
                    <?php endif ?>

            </div>

        </div>

        <button type="submit" class="btn btn-primary">Submit</button>





