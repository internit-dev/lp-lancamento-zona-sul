<?php

return [
    'status' => false,
    'smtpConfig' => [
        'Host'       => 'smtp.gmail.com',
        'Username'   => '',
        'Password'   => '',
        'Port'       => 587,
        'SMTPSecure' => 'tls', // 'tls' ou 'ssl'
        'FromEmail'  => 'naoresponda@gmail.com',
        'FromName'   => 'LP - Padrão',
    ],
    'sendTo' => [
        
        'alan@internit.com.br',
    ],
];    