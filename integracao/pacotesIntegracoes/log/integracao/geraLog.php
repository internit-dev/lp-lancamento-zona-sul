<?php

$logConfiguracoes = require_once '../configuracoes/configuracoes.php';
include_once '../../includeIntegracoes.php';

if( $logConfiguracoes['status'] ):

    $formularioDados = $_REQUEST['formularioDados'];
    $respostaIntegracoes = $_REQUEST['respostaIntegracoes'];


    /** Criação do Cabeçalho */
    $cabecalho = [
        'Data Leed',
        'Fomulario ID',
        'Fomulario Nome',
        'Nome',
        'Email',
        'Telefone Completo',
        'DDD',
        'Telefone Sem DDD',
        'Mensagem',
        'Termos De Uso',
    ];


    /** Criação dos dados do Formulario */
    $dados = [
            'data' => date('d/m/Y H:i'),
            'formId' => $formularioDados['formId'],
            'formNome' => $formularioDados['formNome'],
            'nome' => $formularioDados['nome'],
            'email' => $formularioDados['email'],
            'telefoneCompleto' => $formularioDados['telefoneCompleto'],
            'ddd' => $formularioDados['ddd'],
            'telefoneSemDDD' => $formularioDados['telefoneSemDDD'],
            'mensagem' => $formularioDados['mensagem'],
            'termos' => $formularioDados['termos'],

    ];


    /** Criação das Resposta das Integrações */
    foreach ($integracoes as $integracao){
        if($integracao == 'form' || $integracao == 'log'){ continue; }

        $cabecalho[] = $integracao.' Integração';


        $status = $respostaIntegracoes[$integracao.'Resposta']['status'];
        $resposta = $respostaIntegracoes[$integracao.'Resposta']['mensagem'];


        if($status != '' && $resposta != ''){
            $mensagem = 'Status: '.$status.' && Mensagem: '.$resposta;
        }else{
            $mensagem = 'Não Configurado!';
        }

        $dados = array_merge($dados, [ $integracao.'Reposta' =>  $mensagem ] );

    }

    $a = [ $dados ];



    $nomeArquivo = 'log.csv';

    $arquivo = fopen($nomeArquivo, 'a+');

    if(!fread( $arquivo, filesize($nomeArquivo) )){
        fputcsv($arquivo, $cabecalho);
    }

    foreach ([ $dados ] as $linha) {
        //var_dump($linha);
        fputcsv($arquivo, $linha);
    }

    fclose($arquivo);

endif;

