

    <h1> Informações Registro de Log: </h1>

    <br>


        <div class="row">


                <div class="form-check form-switch mb-3" style="margin-left: 20px;">
                    <input class="form-check-input" type="checkbox" name="status" <?php if($logConfiguracoes['status'] == true): ?>  checked <?php endif ?> >
                    <label class="form-check-label">Status</label>
                </div>

        </div>

        <button type="submit" class="btn btn-primary">Atualizar Configurações</button>


            <br><br><br><br>

            <h2> Registro de Leeds:
                <a class="btn btn-primary" href="pacotesIntegracoes/log/integracao/log.csv" role="button" download>Donwload</a>
            </h2>

            <div class="row">

                <?php

                $nomeArquivo = 'pacotesIntegracoes/log/integracao/log.csv';

                $dados = [];
                if (($handle = fopen($nomeArquivo, "r")) !== FALSE) {
                    while (($data = fgetcsv($handle, 5000, ",")) !== FALSE) {
                        $dados[] = $data;
                    }
                    fclose($handle);
                }

                ?>

                <div class="table-responsive">
                    <table class="table">
                        <caption>Lista de Leeds</caption>
                        <thead>
                            <tr>
                                <?php foreach ($dados[0] as $thead): ?>
                                    <th scope="col"><?= $thead ?></th>
                                <?php endforeach; ?>
                            </tr>
                        </thead>

                        <tbody>
                        <?php unset($dados[0]); foreach ($dados as $linha): ?>
                            <tr>
                                <?php foreach ($linha as $item ): ?>
                                    <td><?= $item ?></td>
                                <?php endforeach; ?>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>

                    </table>
                </div>


            </div>








