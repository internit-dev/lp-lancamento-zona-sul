    Sua house
    <h1> Dados Sua House CRM: </h1>
    <br>


        <div class="row">

            <div class="form-check form-switch mb-3" style="margin-left: 20px;">
                <input class="form-check-input" type="checkbox" value="true" name="status" <?php if($suahouseConfiguracoes['status'] == true): ?> checked <?php endif ?> disabled>
                <label class="form-check-label">Status</label>
            </div>


            <div class="mb-3 col-4">
                <label class="form-label">url</label>
                <input type="text" name="url" class="form-control" value="<?= $suahouseConfiguracoes['url'] ?>" placeholder="">
            </div>

            <div class="mb-3 col-4">
                <label class="form-label">Hc Empreendimento</label>
                <input type="text" name="hc_empreendimento" class="form-control" value="<?= $suahouseConfiguracoes['hc_empreendimento'] ?>" placeholder="">
            </div>

            <div class="mb-3 col-4">
                <label class="form-label">Hc Dominio Chat</label>
                <input type="text" name="hc_dominio_chat" class="form-control" value="<?= $suahouseConfiguracoes['hc_dominio_chat'] ?>" placeholder="">
            </div>

            <div class="mb-3 col-4">
                <label class="form-label">Hc Https</label>
                <input type="text" name="hc_https" class="form-control" value="<?= $suahouseConfiguracoes['hc_https'] ?>" placeholder="">
            </div>

            <div class="mb-3 col-4">
                <label class="form-label">Hc Filial</label>
                <input type="text" name="hc_filial" class="form-control" value="<?= $suahouseConfiguracoes['hc_filial'] ?>" placeholder="">
            </div>

            <div class="mb-3 col-4">
                <label class="form-label">Hc Informacao</label>
                <input type="text" name="hcInformacao" class="form-control" value="<?= $suahouseConfiguracoes['hcInformacao'] ?>" placeholder="">
            </div>

            <div class="mb-3 col-4">
                <label class="form-label">hcCampanha</label>
                <input type="text" name="hcCampanha" class="form-control" value="<?= $suahouseConfiguracoes['hcCampanha'] ?>" placeholder="">
            </div>

            <div class="mb-3 col-4">
                <label class="form-label">Codigo Empreendimento</label>
                <input type="text" name="codigoEmpreendimento" class="form-control" value="<?= $suahouseConfiguracoes['codigoEmpreendimento'] ?>" placeholder="">
            </div>

            <div class="mb-3 col-4">
                <label class="form-label">Codigo Filial</label>
                <input type="text" name="codFilial" class="form-control" value="<?= $suahouseConfiguracoes['codFilial'] ?>" placeholder="">
            </div>

            <div class="mb-3 col-4">
                <label class="form-label">Campanha</label>
                <input type="text" name="campanha" class="form-control" value="<?= $suahouseConfiguracoes['campanha'] ?>" placeholder="">
            </div>

        </div>

        <button type="submit" class="btn btn-primary">Submit</button>

