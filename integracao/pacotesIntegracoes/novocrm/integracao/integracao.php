<?php if($novocrmConfiguracoes['status'] == true): ?>



<script>
/*********************************************************************************************
 * ************************************* Parâmetros ******************************************
 * *******************************************************************************************
 */
    const novoCrmId = '<?= $novocrmConfiguracoes["id"] ?>';


/*
let formularioDados = {
    'formId': formId,
    'formNome': formNome,
    'nome': nome,
    'email': email,
    'telefoneCompleto': telefoneCompleto,
    'telefoneSemDDD': telefoneSemDDD,
    'ddd': ddd,
    'mensagem': mensagem,
    'termos': termos,
  };
*/

/**
 * Função Padrão que Realizará a Integração da Portal do Corretor
 * @param formularioDados
 * @param dispositivo
 * @param parametrosUrl
 */
function novocrmAplicarIntegracao(formularioDados, dispositivo, parametrosUrl)
{
    //alert('Dentro Aplica Integração Novo Crm');
    let resposta = {};

    let response = sendToCRM(formularioDados, dispositivo, parametrosUrl);

    response.always(function(data){

        let status = data['status'];

        resposta = {
            'status': true,
            'mensagem': 'Leed Enviado ',
        };

        if(status == 500){
            resposta = {
                'status': false,
                'mensagem': 'Erro 500, Possivelmente Id Incorreto',
            };
        }

    })

    return resposta;
}



/*********************************************************************************************
 * ************************************** Funções ********************************************
 * *******************************************************************************************
*/

/**
 * 
 * @param {*} formSend 
 * @param {*} device 
 * @param {*} parameters 
 * @param {*} realtyId 
 */
function sendToCRM(formSend, device, parameters) {

  let apiNome = formSend['nome'];
  let apiEmail = formSend['email'];
  let apiTelefone1 = formSend['telefoneCompleto'];
  let empreendimentoId = formSend['empreendimentoId'];
  let crmId = novoCrmId;

  if(formSend['empreendimentoId'] !== undefined){
     crmId = empreendimentoId;
  }

    return $.ajax({
        type: 'POST',
        async: false,
        url: `https://www.novocrm.atendimentoon.com.br/api/form/externo/`+crmId,
        data: {
            'apiNome': formSend['nome'],
            'apiEmail': formSend['email'],
            'apiTelefone1': formSend['telefoneCompleto'],
            'apiDispositivo': dispositivo,
            'utm_source': formSend['utm_source'],
            'utm_medium': formSend['utm_medium'],
            'utm_campaign': formSend['utm_campaign'],
        },
    //data: `apiNome=${apiNome}&apiEmail=${apiEmail}&apiTelefone1=${apiTelefone1}&apiDispositivo=${device}&${parameters}`
  })

}

</script>

<?php endif; ?>