<?php if($junixConfiguracoes['status'] == true): ?>


    <script>

        const junixUrl = '<?= $junixConfiguracoes["url"] ?>',
            junixUsuario = '<?= $junixConfiguracoes["usuario"] ?>',
            junixSenha = '<?= $junixConfiguracoes["senha"] ?>',
            junixIdProduto = '<?= $junixConfiguracoes["idProduto"] ?>';

        /*
         let formularioDados = {
                'formId': formId,
                'formNome': formNome,
                'nome': nome,
                'email': email,
                'telefoneCompleto': telefoneCompleto,
                'telefoneSemDDD': telefoneSemDDD,
                'ddd': ddd,
                'mensagem': mensagem,
                'termos': termos,
              };
        */

        /**
         * Função Padrão que Realizará a Integração da Portal do Corretor
         * @param formularioDados
         * @param dispositivo
         * @param parametrosUrl
         */
        function junixAplicarIntegracao(formularioDados, dispositivo, parametrosUrl)
        {
            //alert('Dentro de Aplica Integracao Junix');

            let resposta = {};

            let produtos = junixGetProdutos();
            console.log(produtos);

            let reponseToken = junixGetToken();

            if(reponseToken['error']){

                resposta = {
                    'status': false,
                    'mensagem': reponseToken['error'] + ' - ' + reponseToken['error_description'],
                };

            }

            if(reponseToken['access_token']){

                responseLeed = junixSendLeed(formularioDados, reponseToken);

                responseLeed.always(function(data){

                    resposta = {
                        'status': data['OK'],
                        'mensagem': 'Id Gerado: ' + data['Dados'].Id + " - Mensagem: " + data['Mensagem'] ,
                    };

                })


            }


            //console.log(resposta)

            return resposta;

        }


        function junixGetProdutos(){

            let token = junixGetToken();

            let response = $.ajax({
                async: false,
                //contentType: "application/json; charset=utf-8",
                type: 'GET',
                url: junixUrl+'/api/produto/listagem',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer '+token['access_token']);
                },
            });

            return response['responseJSON'];
        }


        function junixSendLeed(formularioDados, token){

            let data = {
                "IdExterno": formularioDados['formId'],
                "IdProduto": junixIdProduto,
                "Nome": formularioDados['nome'],
                "Email": formularioDados['email'],
                "Telefone": formularioDados['telefoneCompleto'],
                "Mensagem": formularioDados['mensagem'],
                "CanalTipo": "Email",
                "Midia": "",
                "Origem": "",
                "utm_source": urlParam('utm_source'),
                "utm_campaign": urlParam('utm_campaign'),
                "utm_medium": urlParam('utm_medium'),
            }

            //"utm_source": "string",
            //"utm_campaign": "string",
            //"utm_medium": "string",
            //"utm_content": "string",
            //"utm_term": "string"

            return $.ajax({
                async: false,
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                url: junixUrl+'/api/lead/incluir',
                data: JSON.stringify(data),
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer '+ token['access_token']);
                },
            });

        }


        function junixGetToken(){

            let data ={
                username: junixUsuario,
                Password: junixSenha,
                grant_type: 'password',
            }

            let response = $.ajax({
                async: false,
                contentType: "application/x-www.form-urlenconded",
                type: 'POST',
                url: junixUrl+'/api/token',
                data: data
            });

            return response['responseJSON'];
        }














    </script>



<?php endif; ?>