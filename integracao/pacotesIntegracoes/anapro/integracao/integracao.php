<?php if($anaproConfiguracoes['status'] == true): ?>

<script>
/*********************************************************************************************
 * ************************************* Parâmetros ******************************************
 * *******************************************************************************************
 */

/**
 let formularioDados = {
        'formId': formId,
        'formNome': formNome,
        'nome': nome,
        'email': email,
        'telefoneCompleto': telefoneCompleto,
        'telefoneSemDDD': telefoneSemDDD,
        'ddd': ddd,
        'mensagem': mensagem,
        'termos': termos,
    };
 **/


const CampanhaKey = '<?= $anaproConfiguracoes["CampanhaKey"] ?>',
      ProdutoKey = '<?= $anaproConfiguracoes["ProdutoKey"] ?>',
      Key = '<?= $anaproConfiguracoes["Key"] ?>',
      KeyIntegradora = "<?= $anaproConfiguracoes["KeyIntegradora"] ?>",
      KeyAgencia = "<?= $anaproConfiguracoes["KeyAgencia"] ?>",
      CanalKey = "<?= $anaproConfiguracoes["CanalKey"] ?>",
      CampanhaPeca = urlParam('utm_campaign'),
      Midia = urlParam('utm_source'),
      Peca = urlParam('utm_medium');

/*********************************************************************************************
 * ************************************** Funções ********************************************
 * *******************************************************************************************
*/


/**
 *
 * @param formularioDados
 * @param dispositivo
 * @param parametrosUrl
 * @return {string}
 */
function anaproAplicarIntegracao(formularioDados, dispositivo, parametrosUrl)
{
    //alert("Aplica Integração Anapro");
    let resposta = {};

    let keys = selectKeys(formularioDados);

    let response = sendToAnaPro(formularioDados['nome'], formularioDados['ddd'], formularioDados['telefoneSemDDD'], formularioDados['email'], formularioDados['mensagem'], keys);

    response.always(function(data) {

        resposta = {
            'status': data['Sucesso'],
            'mensagem': data['Mensagem'],
        };

    })

    return resposta;

}


function selectKeys(formularioDados){

    let keys = {
        '199': {
            'CampanhaKey': '75FtzJRnu4s1',
            'ProdutoKey': 'fwjqAv-IVTQ1',
            'Key': 'zb3W3IC4wqI1',
            'KeyIntegradora': '3FE49887-F31C-4E33-88AE-08047FE8ABDF',
            'KeyAgencia': '3FE49887-F31C-4E33-88AE-08047FE8ABDF',
            'CanalKey': '32j6KY46Q_c1',
        },
        '200': {
            'CampanhaKey': '75FtzJRnu4s1',
            'ProdutoKey': 'BgG_4OMHq641',
            'Key': 'zb3W3IC4wqI1',
            'KeyIntegradora': '3FE49887-F31C-4E33-88AE-08047FE8ABDF',
            'KeyAgencia': '3FE49887-F31C-4E33-88AE-08047FE8ABDF',
            'CanalKey': '32j6KY46Q_c1',
        },
        '201': {
            'CampanhaKey': '75FtzJRnu4s1',
            'ProdutoKey': 'kaLBBL4fpgY1',
            'Key': 'zb3W3IC4wqI1',
            'KeyIntegradora': '3FE49887-F31C-4E33-88AE-08047FE8ABDF',
            'KeyAgencia': '3FE49887-F31C-4E33-88AE-08047FE8ABDF',
            'CanalKey': '32j6KY46Q_c1',
        },
        '202': {
            'CampanhaKey': '75FtzJRnu4s1',
            'ProdutoKey': 'EAOlYRLmjow1',
            'Key': 'zb3W3IC4wqI1',
            'KeyIntegradora': '3FE49887-F31C-4E33-88AE-08047FE8ABDF',
            'KeyAgencia': '3FE49887-F31C-4E33-88AE-08047FE8ABDF',
            'CanalKey': '32j6KY46Q_c1',
        },
        '203': {
            'CampanhaKey': '75FtzJRnu4s1',
            'ProdutoKey': 'MNyZjZden6A1',
            'Key': 'zb3W3IC4wqI1',
            'KeyIntegradora': '3FE49887-F31C-4E33-88AE-08047FE8ABDF',
            'KeyAgencia': '3FE49887-F31C-4E33-88AE-08047FE8ABDF',
            'CanalKey': '32j6KY46Q_c1',
        }

    }

    return keys[parseInt(formularioDados['empreendimentoId'])];

}

function sendToAnaPro(nome, ddd, telefone, email, mensagem, keys)
{
  
  dados = {
    "Key": keys['Key'],
    "TagAtalho": "",
    "CampanhaKey": keys['CampanhaKey'],
    "ProdutoKey": keys['ProdutoKey'],
    "CanalKey": keys['CanalKey'],
    "Midia": urlParam('utm_source'),
    "Peca": urlParam('utm_medium'),
    "GrupoPeca": "",
    "CampanhaPeca": urlParam('utm_campaign'),
    "PessoaNome": nome,
    "PessoaSexo": "",
    "PessoaEmail": email,
    "PessoaTelefones": [{
        "Tipo": "OUTR",
        "DDD": ddd,
        "Numero": telefone,
        "Ramal": null
    }],
    "Observacoes": mensagem,
    "Status": "",
    "KeyExterno": "",
    "KeyIntegradora": keys['KeyIntegradora'],
    "KeyAgencia": keys['KeyAgencia']
  };

    return jQuery.ajax({
    url: 'https://crm.anapro.com.br/webcrm/webapi/integracao/v2/CadastrarProspect',
    data: dados,
    async: false,
    crossDomain: true,
    cache: false,
    type: 'POST',
    dataType: 'json'
        /*
    success: function (response) {
      //console.log(response);
      return true;
    },
    error: function(result) {
      //console.log(result);
      return false;
    }
    */
  });

}
</script>

<?php endif; ?>