
<?php if($gtagConfiguracoes['status'] == true): ?>


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=<?= $gtagConfiguracoes['id'] ?>"></script>
<script>

    /**
     let formularioDados = {
        'formId': formId,
        'formNome': formNome,
        'nome': nome,
        'email': email,
        'telefoneCompleto': telefoneCompleto,
        'telefoneSemDDD': telefoneSemDDD,
        'ddd': ddd,
        'mensagem': mensagem,
        'termos': termos,
    };
     **/

    /**
     * Função Padrão que Realizará a Integração da Anapro
     * @param formularioDados
     * @param dispositivo
     * @param parametrosUrl
     */
    function gtagAplicarIntegracao(formularioDados, dispositivo, parametrosUrl)
    {
        //alert('GTag Aplicar Integração')
        let resposta = {};

        sendGtag(formularioDados['formNome'], formularioDados['empreendimento']);

        resposta = {
            'status': true,
            'mensagem': 'Sem Resposta Padrão - Validar No CRM',
        };

        return resposta;
    }


    /**
     *
     * @param event
     * @param event_label
     */
    function sendGtag(event, event_label)
    {

         gtag('event', 'enviar', {
             'event_category': 'formulario',
             'event_label': event_label,
         });

    }

      window.dataLayer = window.dataLayer || [];
      function gtag() {
        dataLayer.push(arguments);
      }
      gtag('js', new Date());
      gtag('config', '<?= $gtagConfiguracoes['id'] ?>');

    </script>

<?php endif; ?>