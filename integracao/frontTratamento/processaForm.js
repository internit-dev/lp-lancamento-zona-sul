/** DADOS DO OBJETO FORMULARIO DADOS

let formularioDados = {
    'formId': formId,
    'formNome': formNome,
    'nome': nome,
    'email': email,
    'telefoneCompleto': telefoneCompleto,
    'telefoneSemDDD': telefoneSemDDD,
    'ddd': ddd,
    'mensagem': mensagem,
    'termos': termos,
};
 */

/**
 * É EXECUTADA ANTES DO PROCESSAMENTO/INTEGRACOES DO FORMULARIO
 *
 * @param formularioDados - Recebe um objeto que possui todads as informações do formulario
 * @return {*[]} - Retorna Array que é recebido como parametro na função formularioProcessado()
 */
function frontFormularioProcessando(formularioDados){


    $("#form-botao-"+formularioDados['formNome']).prop("disabled",true);

    $('#modal-resposta-texto').html('Enviando Informações!');
    $('#modal-resposta').modal('show')


    return [ ];
}


/**
 * É EXECUTADO APOS O PROCESSAMENTO/INTEGRACOES DO FORMULARIO
 *
 * @param argsFormProcessando - Argumentos Retornados Pela Função formularioProcessando()
 * @param formularioDados - Traz Todas as Informação do Formulario
 * @param statusIntegracao - { TRUE OR FALSE } Vai Retornar Se O Formulario Foi Processado Corretamente
 */
function frontFormularioProcessado(argsFormProcessando, formularioDados, statusIntegracao){


    $("#form-botao-"+formularioDados['formNome']).prop("disabled", false);
    $("#"+formularioDados['formId'])[0].reset();

    if(statusIntegracao){

        $('#modal-resposta-texto').html('Contato Realizado Com Sucesso!');
        $('#modal-resposta').modal('show');

    }else{

        $('#modal-resposta-texto').html('Não Foi Possivel Entrar Em Contato no Momento! <br><br> Tente Novamente Mais Tarde! ');
        $('#modal-resposta').modal('show');

    }

}



/**
 * Função de call back para fazer validação visual de Email para o Usuario
 * Roda toda vez que usuario seleciona o input de Email
 *
 * @param labelEmail - Referencia do input JQUERY
 * @param status - { TRUE OR FALSE } Vai Referenciar se o email preenchido pelo usuario é valido ou não
 */
function frontValidacaoEmail(labelEmail, status){

    if(status){

        labelEmail.css('border', '');

    }else{

        labelEmail.focus();
        labelEmail.css('border', '1px red solid');

    }


}


/**
 * Função de call back para fazer validação visual de Telefone para o Usuario
 * Roda toda vez que usuario seleciona o input de Telefone
 *
 * @param labelTelefone - Referencia do input JQUERY
 * @param status - { TRUE OR FALSE } Vai Referenciar se o telefone preenchido pelo usuario é valido ou não
 */
function frontValidacaoTelefone(labelTelefone, status){

    if(status){

        labelTelefone.css('border', '');

    }else{

        labelTelefone.focus();
        labelTelefone.css('border', '1px red solid');

    }

}
