<?php
    include "frontTratamento/frontIncludes.php";


    /** Arquivo a ser incluido dentro da landPage Padrãp */


    /** Importa Arquivos que possui o registro das Integrações */
    require_once 'pacotesIntegracoes/includeIntegracoes.php';


    /** Importa Arquivos que auxiliam nas integrações */
    require 'aplicaIntegracoes/auxiliares/includeAuxiliares.php';


    /** Importa todas as Configurações das integrações Registradas  */
    foreach ($integracoes as $integracao){
        $varName = $integracao . 'Configuracoes';
        $$varName = include_once 'pacotesIntegracoes/' . $integracao . '/configuracoes/configuracoes.php';
    }

    /** Importa todas os arquivos de integrações Registradas  */
    foreach ($integracoes as $integracao){
        if( in_array($integracao, $integracoesExcecao) ){ continue; }
        $$integracao = include_once 'integracao/pacotesIntegracoes/' . $integracao . '/integracao/integracao.php';
    }


    //require 'integracao/aplicaIntegracoes/processa.php';
    require 'integracao/aplicaIntegracoes/aplicaIntegracao.php';


