<?php

session_start();

if( !isset($_SESSION['login']) ){ header("location:login.php"); }


require_once 'pacotesIntegracoes/includeIntegracoes.php';

foreach ($integracoes as $integracao){
    $varName = $integracao . 'Configuracoes';
    $$varName = include_once 'pacotesIntegracoes/' . $integracao . '/configuracoes/configuracoes.php';;
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Alan e Cruvello">
    <meta name="generator" content="Hugo 0.83.1">
    <title>Sidebars · Bootstrap v5.0</title>


    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/sidebars.css" rel="stylesheet">
    <!-- Custom styles for this template -->
</head>
<body>

<main>

    <div class="d-flex flex-column flex-shrink-0 p-3 bg-light" style="width: 280px;">

        <a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto link-dark text-decoration-none">
            <svg class="bi me-2" width="40" height="32"><use xlink:href="#bootstrap"/></svg>
            <span class="fs-4">Integrações LP</span>
        </a>

        <hr>

        <ul class="nav nav-pills flex-column mb-auto" id="pills-tab" role="tablist">

            <li class="nav-item" role="presentation">
                <a class="nav-link" id="pills-geral-tab" data-bs-toggle="pill" data-bs-target="#pills-geral" type="button" role="tab" aria-controls="pills-geral" aria-selected="true">
                    Geral
                </a>
            </li>


            <?php foreach ($integracoes as $integracao): // Percorre o registro de Integrações e  ?>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="pills-<?= $integracao ?>-tab" data-bs-toggle="pill" data-bs-target="#pills-<?= $integracao ?>" type="button" role="tab" aria-controls="pills-<?= $integracao ?>" aria-selected="true">
                        <?= $integracao ?>
                    </a>
                </li>
            <?php endforeach; ?>



        </ul>

        <hr>

        <!-- Default dropup button -->
        <div class="btn-group dropup">
            <button type="button" class="btn btn-secondary dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
                Dropup
            </button>
            <ul class="dropdown-menu">
                <li><button class="dropdown-item" type="button">Action</button></li>
                <li><button class="dropdown-item" type="button">Another action</button></li>
                <li><button class="dropdown-item" type="button">Something else here</button></li>
            </ul>
        </div>




    </div>



    <div class="tab-content" id="pills-tabContent">


        <div class="tab-pane fade active" id="pills-geral" role="tabpanel" aria-labelledby="pills-geral-tab">
            Geral
        </div>

        <?php foreach ($integracoes as $integracao): ?>
            <div class="tab-pane fade" id="pills-<?= $integracao ?>" role="tabpanel" aria-labelledby="pills-<?= $integracao ?>-tab">
                <form method="GET" action="pacotesIntegracoes/<?= $integracao ?>/controller/controller.php">


                    <?php include 'pacotesIntegracoes/' . $integracao . '/template/form.php'; ?>


                </form>
            </div>
        <?php endforeach; ?>


    </div>







</main>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>

<script src="assets/js/sidebars.js"></script>

<?php
    $tab = $_REQUEST['tab'];
    if(is_null($tab)){
        $tab = 'geral';
    }
?>

<script>
    $(document).ready(function() {

        $('#pills-<?= $tab ?>-tab').tab('show');
    });

</script>


</body>
</html>




