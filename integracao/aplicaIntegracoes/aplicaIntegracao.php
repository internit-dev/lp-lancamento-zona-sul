<script>

    /*******************************************************************************************
     * ***************** Gera Script de Processamento para cada form Registrado ****************
     * ************************************************************************************** */

    $(document).ready(function() {

        <?php foreach ( $formConfiguracoes as $form ): ?>

            $('#form-<?= $form ?>').submit(function(e) {

                e.preventDefault();

                const form = $(this).closest('form');
                const count = checkNotNull(form.attr('id'));

                if (count == 0) {
                    aplicaIntegracoes(form);
                }

            });

        <?php endforeach; ?>

        /** Validada Email E Chama Call Back Pro Front Tratar */
        $("[name='Email']").blur(function() {
            frontValidacaoEmail( $(this), validacaoTelefone($(this).val()) );
        });

        /** Validada Telefone E Chama Call Back Pro Front Tratar */
        $("[name='Telefone']").blur(function() {
            frontValidacaoTelefone( $(this), validacaoTelefone($(this).val()) );
        });


    });


    /*******************************************************************************************
     * *************************** Função que chama as integrações *****************************
     * *****************************************************************************************
     */

    /** @param {*} form */
    function aplicaIntegracoes(form)
    {

        /** Pega Todas As Informações Do Fomulario Enviado e Joga Em Variaveis */
        let formId = form.attr('id');
        let formNome = formId.replace('form-', '');
        let nome = $(`#${formId} [name="Nome"]`).val();
        let telefoneCompleto = $(`#${formId} [name="Telefone"]`).val();
        let telefoneSemDDD = telefoneCompleto != null ? telefoneCompleto.substr(5, telefoneCompleto.length) : '';
        let ddd = telefoneCompleto != null ? telefoneCompleto.substr(1, 2) : '';
        let email = $(`#${formId} [name="Email"]`).val();
        let mensagem = $(`#${formId} [name="Mensagem"]`).val();
        let termos = $(`#${formId} [name="Termos"]`).val();
        let empreendimento =$(`#${formId} [name="empreendimento"]`).val();

        /** VALIDAR Empreendimento */
        if( empreendimento == 0 ){ alert("Selecione um empreendimento"); return; }

        empreendimento = empreendimento.split("-");
        let empreendimentoId = empreendimento[0];
        empreendimento = empreendimento[1];

        //alert(empreendimento);
        /** VALIDAR QUANTIDADE CARACTERES TELEFONE */
        if( !validacaoTelefone(telefoneCompleto) ){ return; }

        /** VALIDAR EMAIL */
         if( !validacaoEmail( email ) ){ return; }

        /** Pega Informações Do Formulario E constroi Um objeto Com Os Dados  */
        let formularioDados = {
            'formId': formId,
            'formNome': formNome,
            'nome': nome,
            'email': email,
            'telefoneCompleto': telefoneCompleto,
            'telefoneSemDDD': telefoneSemDDD,
            'ddd': ddd,
            'mensagem': mensagem,
            'termos': termos,
            'empreendimento': empreendimento,
            'empreendimentoId': empreendimentoId,
            'utm_source': urlParam('utm_source'),
            'utm_medium': urlParam('utm_medium'),
            'utm_campaign': urlParam('utm_campaign'),
        };

        /** CALL BACK PRO FRONT - PROCESSANDO FORMULARIO */
        let argsFormProcessando = frontFormularioProcessando(formularioDados);


        /** **************************************************************************************************************************************************** */
        /** Percorre as integracoes */
        <?php
        foreach ($integracoes as $indice => $integracao):
            $integracaoConfiguracoes = $integracao . 'Configuracoes';
            if( $$integracaoConfiguracoes['status'] == true ):
            if($integracaoConfiguracoes == 'logConfiguracoes'){ continue; }
        ?>

        /**
         * Funcão Que Envia Leed Para A Integração <?= $integracao ?> E Retorna Uma Reposta True Ou False Para A Variavel <?= $integracao . 'Resposta' ?>
         */
        let <?= $integracao . 'Resposta' ?> = <?= $integracao ?>AplicarIntegracao(formularioDados, dispositivo, parametrosUrl );

        <?php endif; endforeach; ?>
        /** **************************************************************************************************************************************************** */



        /** **************************************************************************************************************************************************** */
        /** Percorre as integracoes */
        let respostaIntegracoes = {
            <?php
            foreach ($integracoes as $indice => $integracao):
                if( in_array($integracao, $integracoesExcecao) ){ continue; }

                $integracaoConfiguracoes = $integracao . 'Configuracoes';
                if( $$integracaoConfiguracoes['status'] == true ):
                if($integracaoConfiguracoes == 'logConfiguracoes'){ continue; }
            ?>

            <?= $integracao . 'Resposta' ?>,

            <?php endif; endforeach; ?>
        };
        /** **************************************************************************************************************************************************** */

        /** Chamo o gerador de log */

        <?php if( $logConfiguracoes['status']): ?>
            logAplicarIntegracao(formularioDados, dispositivo, parametrosUrl, respostaIntegracoes)
        <?php endif; ?>


        /** Chama funcao que valida quantas integracoes foram realizadas .... */
        let statusIntegracao = validaIntegracoesRealizadas(respostaIntegracoes)


        /** CALL BACK PRO FRONT - FORMULARIO PROCESSADO */
        frontFormularioProcessado(argsFormProcessando, formularioDados, statusIntegracao);


    }

</script>