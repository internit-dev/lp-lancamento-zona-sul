$(document).ready(function(){

  
  	// ADD CLASS MENU
	jQuery(function () {
	  jQuery(window).scroll(function () {
	    if (jQuery(this).scrollTop() > 700) {
       $("#menu").addClass("menu_down");
	     $(".social").hide();
	    } else {
	     $("#menu").removeClass("menu_down");
       $(".social").show();
	    }
	  });
	});


	// SCROLL ANIMATE
	var $doc = $('html, body');
	$('a.scroll').click(function() {
    $doc.animate({
        scrollTop: $( $.attr(this, 'href') ).offset().top
    }, 1000);
    return false;
	});

});


$( document ).ready(function() {      
    var isMobile = window.matchMedia("only screen and (max-width: 760px)");
    if (isMobile.matches) {
        $('#navbar a').click(function(){
          $(".navbar-collapse").hide();
        });
    }
    $('.navbar-toggle').click(function(){
          $(".navbar-collapse").show();
    });
});