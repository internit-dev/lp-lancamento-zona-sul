<html>

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
  <meta name="generator" content="Jekyll v4.1.1">
  <meta name="facebook-domain-verification" content="qxxpxmxr5mx8ma53e7h9plaijgzh8r" />
  <title>Imóveis na Zona Sul Rio de Janeiro para comprar ou investir Apartamentos de 2, 3 e 4 Quartos.</title>

  <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/carousel/">

  <!-- Bootstrap core CSS -->
  <link href="assets/dist/css/bootstrap.min.css" rel="stylesheet">

  <!-- fontawesome -->
  <link href="fontawesome/css/all.css" rel="stylesheet">

  <style>
  .bd-placeholder-img {
    font-size: 1.125rem;
    text-anchor: middle;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
  }

  @media (min-width: 768px) {
    .bd-placeholder-img-lg {
      font-size: 3.5rem;
    }
  }
  </style>

  <!-- STYLE -->
  <!-- Owl Stylesheets -->
  <link rel="stylesheet" href="style/lightbox/css/lightbox.min.css">
  <link rel="stylesheet" href="assets/owlcarousel/assets/owl.carousel.min.css">
  <link rel="stylesheet" href="assets/owlcarousel/assets/owl.theme.default.min.css">

  <link rel="stylesheet" href="style/owlslider/owl.carousel.min.css" />
  <link rel='stylesheet' href='style/style.css' media='all' />
  <link rel='stylesheet' href='style/mobile.css' media='all' />

  <!-- javascript -->
  <!-- <script src="../assets/vendors/jquery.min.js"></script> -->
  <!-- <script src="../assets/owlcarousel/owl.carousel.js"></script> -->



  <!-- FONT -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link
    href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap"
    rel="stylesheet">

  <?php
$vUrl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

$nome     = '';
$email    = '';
$tel      = '';
$msg	  = '';
if (isset($_POST['nome'])) {
$nome     = $_POST['nome'];
$email    = $_POST['email'];
$tel      = $_POST['telefone'];
$msg	  = $_POST['mensagem'];
}

?>
  <script language="javascript" type="text/javascript">
  function checa(nform) {

    $('#enviar').hide();

    if (formEmail.nome.value == "") {

      alert("Preencha o Campo 'Nome'");

      formEmail.nome.focus();

      formEmail.nome.select();

      $('#enviar').show();
      return false;

    }

    if (formEmail.email.value == "") {

      alert("Preencha o Campo 'Email'");

      formEmail.email.focus();

      formEmail.email.select();

      $('#enviar').show();
      return false;

    }

    var oRegEmail = /^[a-z0-9\._\-]+\@[a-z0-9\._\-]+\.[a-z]{2,3}$/i;

    if (!oRegEmail.test(formEmail.email.value)) {
      alert('Por favor, confira seu endereço de e-mail.');
      formEmail.email.focus();
      formEmail.email.select();
      $('#enviar').show();
      return false;
    }

    if (formEmail.telefone.value == "") {

      alert("Preencha o Campo 'Telefone'");

      formEmail.telefone.focus();

      formEmail.telefone.select();

      $('#enviar').show();
      return false;

    }

    var str = new String(formEmail.telefone.value);
    if (str.length < 14) {
      alert("Preencha o Campo 'Telefone' completo");
      formEmail.telefone.focus();
      formEmail.telefone.select();
      $('#enviar').show();
      return false;
    }

    if (formEmail.empreendimento.value == "0") {

      alert("Selecione o 'Empreendimento'");

      formEmail.empreendimento.focus();

      formEmail.empreendimento.select();

      $('#enviar').show();
      return false;

    }

    $('#enviar').show();
    formEmail.action = 'obrigado.php';
    formEmail.method = 'post';
    formEmail.submit();
    return true;

  }



  function mask(o, f) {
    setTimeout(function() {
      var v = mphone(o.value);
      if (v != o.value) {
        o.value = v;
      }
    }, 1);
  }

  function mphone(v) {
    var r = v.replace(/\D/g, "");
    r = r.replace(/^0/, "");
    if (r.length > 10) {
      r = r.replace(/^(\d\d)(\d{5})(\d{4}).*/, "($1) $2-$3");
    } else if (r.length > 5) {
      r = r.replace(/^(\d\d)(\d{4})(\d{0,4}).*/, "($1) $2-$3");
    } else if (r.length > 2) {
      r = r.replace(/^(\d\d)(\d{0,5})/, "($1) $2");
    } else {
      r = r.replace(/^(\d*)/, "($1");
    }
    return r;
  }
  </script>




  <!-- Facebook Pixel Code -->
  <script>
  ! function(f, b, e, v, n, t, s) {
    if (f.fbq) return;
    n = f.fbq = function() {
      n.callMethod ?
        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
    };
    if (!f._fbq) f._fbq = n;
    n.push = n;
    n.loaded = !0;
    n.version = '2.0';
    n.queue = [];
    t = b.createElement(e);
    t.async = !0;
    t.src = v;
    s = b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t, s)
  }(window, document, 'script',
    'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '133980318856897');
  fbq('track', 'PageView');
  </script>

  <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=133980318856897&ev=PageView&noscript=1" /></noscript>

  <!-- End Facebook Pixel Code -->


</head>

<body>


  <div class="menulateral">
    <div class="box email">
      <a class="scroll" href="#header">
        <i class="fas fa-envelope"></i>
        <span>Informações de venda</span>
      </a>
    </div>
    <div class="box chat">
      <a id="open-anapro" href="#">
        <i class="far fa-comments"></i>
        <span>Chat Vendas</span>
      </a>
    </div>
    <div class="box phone">
      <a onclick="sendGtag('whatsapp', 'whatsapp'); sendFbq('whatsapp')"
        href="https://api.whatsapp.com/send?phone=5521971540174"
        target="_blank">
        <img src="style/images/icon_whatsapp.png" class="icon_whatsapp" />
        <span>Whatsapp</span>
      </a>
    </div>
  </div>


  <div class="header" id="header">
    <div class="container">

      <div class="row">
        <div class="col-md-12">
          <h1>Morar na Zona Sul.</h1>
          <h2>As melhores opções para morar ou investir em imóveis.</h2>
        </div>
      </div>
      <!--ROW-->

      <div class="boxinfo">
        <div class="row">
          <div class="col-md-6">

            <div class="escolha">
              <h1>Escolha onde quer morar:</h1>
              <a class="scroll" href="#jardim_botanico">Jardim Botânico</a>
              <a class="scroll" href="#botafogo">Botafogo</a>
              <a class="scroll" href="#lagoa">Lagoa</a>
            </div>

          </div>
          <div class="col-md-6">

            <div class="contato" id="contato">
              <div class="container">
                <div class="form">
                  <h1>Quero mais Informações</h1>



                  <?php ############################################################################################################# ?>
                  <?php $formName = 'banner'; ?>
                  <form action="" name="<?= $formName ?>" id="form-<?= $formName ?>">

                    <!-- input nome -->
                    <input name="Nome" type="text" class="inp email" placeholder="Nome*" required>

                    <!-- input email -->
                    <input name="Email" type="email" class="inp email" placeholder="E-mail*" required>

                    <!-- input telefone -->
                    <input name="Telefone" type="tel" class="phone inp email" placeholder="Telefone*" MaxLength="15"
                      onKeyDown="mask(this, mphone);" onKeyPress="mask(this, mphone);" onKeyUp="mask(this, mphone);"
                      required>

                    <select name="empreendimento" class="inp email" id="empreendimento" required>
                      <option value="0">Empreendimento</option>
                      <option value="202 - lineu_paula">Lineu 708</option>
                      <option value="200 - conde_iraja">S Design</option>
                      <option value="199 - jardim_botafogo">Jardim Botafogo</option>
                      <option value="201 - borges">Borges 3647</option>
                    </select>

                    <!-- input Mensagem -->
                    <textarea name="Mensagem" type="text" placeholder="Mensagem" rows="6" cols="50"></textarea>

                    <!-- input checkbox -->
                    <p style="color: white;"><b>
                        <input name="Termos" type="checkbox" value="aceito" required>Eu li e aceito os termos e
                        <a href="politica-privacidade" target="_blank" style="color: white;">Política de
                          Privacidade</a>.</b>
                    </p>

                    <!-- botão enviar -->
                    <button id="form-botao-<?= $formName ?>" class="bt" type="submit">Enviar</button>

                  </form>
                  <?php unset($formName); ?>
                  <?php ############################################################################################################# ?>





                </div>
              </div>
            </div><!-- CONTATO -->

            <br class="clear" />

          </div>
        </div><!-- ROW -->
      </div>
      <!--BOX INFO -->

    </div>
    <!--CONTAINER -->

  </div>
  <!--HEADER-->


  <div class="boxcont boxcont_branco" id="jardim_botanico">

<div class="container">

  <div class="row">
    <div class="col-md-12">
      <h1 class="titulo">
        <span></span><span></span><span></span><span></span>
        Jardim Botânico
        <span></span><span></span><span></span><span></span>
      </h1>
    </div>
  </div>
  <!--ROW-->

  <div class="fotoprincipal">
    <div class="row">
      <div class="col-md-12">
        <img class="foto" src="style/images/fachada_jardim_botanico1.jpg" />
        <span>Lineu 708</span>
      </div>
    </div>
    <!--ROW-->
  </div>

  <div class="infopredio">
    <div class="row">
      <div class="col-md-5">
        <p>
        A tranquilidade da Lagoa ao lado do Jardim Botânico proporciona um estilo único de vida.
        Localizado em uma das melhores ruas da zona Sul, o Lineu 708 entrega aos moradores um
        amplo comércio local que conta com: restaurantes, bares, farmácias, lazer e muito mais.
        O projeto foi desenvolvido para que todas as plantas sejam confortáveis e amplas, onde
        você cria o seu apartamento ideal junto com o escritório de arquitetura PKB.
        </p>
        <div class="fichatec">
          <strong>DISPONIBILIDADE</strong>
          <br />
          10 unidades com 4 suítes, de 214,60m² a 229,85m²
          <br>
          2 up gardens DE 4 SUÍTES DE 254,50 M2 E 297,35 M2
          <br>
          1 cobertura linear de 4 suítes , com 448,50m²
        </div>
        <a href="#header" class="queromaisinfo scroll">Quero mais informações</a>
        <a href="https://www.vguedes.com.br/lineu360/index.html" target="_blank"
              class="queromaisinfo scroll botaosdesign" style="background-color:#333">Tour Virtual do Decorado</a>
      </div>

      <div class="col-md-7">

        <div class="foto01" style="background-image: url(style/images/foto_jardim_botanico_1.jpg);"></div>
        <div class="foto02_jardim_botanico" style="background-image: url(style/images/foto_jardim_botanico_2.jpg);"></div>
        <div class="foto03_jardim_botanico" style="background-image: url(style/images/foto_jardim_botanico_3.jpg);"></div>
        <div class="foto04_jardim_botanico" style="background-image: url(style/images/foto_jardim_botanico_4.jpg);"></div>
      </div>


    </div>
    <!--ROW-->
  </div>

</div>
<!--CONTAINER -->

</div><!-- BOX CONT -->


  <div class="boxcont" id="botafogo">

    <div class="container">

      <div class="row">
        <div class="col-md-12">
          <h1 class="titulo">
            <span></span><span></span><span></span><span></span>
            Botafogo
            <span></span><span></span><span></span><span></span>
          </h1>
        </div>
      </div>
      <!--ROW-->

      <div class="fotoprincipal">
        <div class="row">
          <div class="col-md-12">
            <img class="foto" src="style/images/foto_sdesign_01.jpg" />
            <span>S Design - 3 suítes e 4 quartos</span>
          </div>
        </div>
        <!--ROW-->
      </div>

      <div class="infopredio">
        <div class="row">
          <div class="col-md-5">
            <p>
              Apartamentos tipo de 3 suítes e 4 quartos, entregues com varandas gourmet, churrasqueira a gás, cortina de
              vidro com fechamento retrátil e 2 vagas. Exclusivas coberturas lineares com 4 suítes, entregues com
              piscina, deck, churrasqueira a carvão e 3 vagas.
              <br><br>
              Residencial exclusivo na melhor localização de Botafogo, no trecho mais tranquilo da rua Conde de Irajá e
              a pouca distância de bares e restaurantes badalados, como o Lasai, o Meza Bar e o Oteque, além da Cobal do
              Humaitá, de escolas, supermercados, academias.
              <br><br>
              Uma combinação perfeita entre qualidade de vida, conforto e paz no meio de uma das áreas mais
              efervescentes do Rio.
              <!-- O condomínio com a maior estrutura de lazer de Botafogo, que conduz para o bairro um empreendimento com design contemporâneo, que possuirá entretenimentos individualizados, segurança, sustentabilidade e tecnologia.
          <br /><br />
          Exclusivos apartamentos de 2, 3 e 4 quartos, de 76m² até 200m², que encantam pela sua sofisticação e vistas únicas! -->
            </p>
            <div class="fichatec">
              <strong>DISPONIBILIDADE</strong>
              <br />
              36 Unidades de 150 M² à 166 M²<br>
              9 Unidades de até 4 quartos de 166 M²<br>
              2 Coberturas Lineares 250 M² a 270 M²
            </div>
            <a href="#header" class="queromaisinfo scroll botaosdesign">Quero mais informações</a>
            <a href="https://studiovir.com.br/sdesign360/tour.html" target="_blank"
              class="queromaisinfo scroll botaosdesign" style="background-color:#333">Tour Virtual do Decorado</a>
            <!-- <a href="https://www.youtube.com/watch?v=rFmMZbWFqog" target="_blank"
              class="queromaisinfo scroll botaosdesign" style="background-color:#333">Vídeo do Empreendimento</a> -->

          </div>

          <div class="col-md-7">

            <div class="foto01" style="background-image: url(style/images/foto_sdesign_02.jpg);"></div>
            <div class="foto02" style="background-image: url(style/images/foto_sdesign_03.jpg);"></div>
            <div class="foto03" style="background-image: url(style/images/foto_sdesign_04.jpg);"></div>
          </div>


        </div>
        <!--ROW-->
      </div>
      <!--infopredio-->

      <div class="fotoprincipal" id="jardim-botafogo">
        <div class="row">
          <div class="col-md-12">
            <img class="foto" src="style/images/foto_botafogo_01.jpg" />
            <span>Jardim Botafogo - 2, 3 e 4 Quartos</span>
          </div>
        </div>
        <!--ROW-->
      </div>

      <div class="infopredio">
        <div class="row">
          <div class="col-md-5">
            <p>
              O condomínio Jardim Botafogo une natureza, conforto e sofisticação em uma área de mais de 8 mil m² com
              toda a estrutura que a sua família precisa! Você merece viver em um dos melhores lançamentos da Zona Sul!
              Saiba mais sobre essa oportunidade incrível.

              <!-- O condomínio com a maior estrutura de lazer de Botafogo, que conduz para o bairro um empreendimento com design contemporâneo, que possuirá entretenimentos individualizados, segurança, sustentabilidade e tecnologia.
          <br /><br />
          Exclusivos apartamentos de 2, 3 e 4 quartos, de 76m² até 200m², que encantam pela sua sofisticação e vistas únicas! -->
            </p>
            <div class="fichatec">
              <strong>DISPONIBILIDADE</strong>
              <br />
              13 Unidades de 2qts<br />
              17 Unidades de 3qts
            </div>
            <a href="#header" class="queromaisinfo scroll">Quero mais informações</a>
            <a href="https://my.matterport.com/show/?m=sxvCSESj94m" target="_blank" class="queromaisinfo scroll"
              style="background-color:#333">Tour Virtual do Decorado</a>
          </div>

          <div class="col-md-7">

            <div class="foto01" style="background-image: url(style/images/foto_botafogo_02.jpg);"></div>
            <div class="foto02" style="background-image: url(style/images/foto_botafogo_03.jpg);"></div>
            <div class="foto03" style="background-image: url(style/images/foto_botafogo_04.jpg);"></div>
          </div>


        </div>
        <!--ROW-->
      </div>
      <!--infopredio-->


    </div>
    <!--CONTAINER -->

  </div><!-- BOX CONT -->


  <div class="boxcont boxcont_branco" id="lagoa">

    <div class="container">

      <div class="row">
        <div class="col-md-12">
          <h1 class="titulo">
            <span></span><span></span><span></span><span></span>
            Lagoa
            <span></span><span></span><span></span><span></span>
          </h1>
        </div>
      </div>
      <!--ROW-->

      <div class="fotoprincipal">
        <div class="row">
          <div class="col-md-12">
            <img class="foto" src="style/images/foto_lagoa_01.jpg" />
            <span>Borges 3647</span>
          </div>
        </div>
        <!--ROW-->
      </div>

      <div class="infopredio">
        <div class="row">
          <div class="col-md-5">
            <p>
              Localizado na região da Lagoa, no Rio de Janeiro o projeto teve como premissa a valorização e integração
              com a paisagem marcante, através da presença de linhas que percorrem toda a fachada.
              <br /><br />
              Um empreendimento único e uma localização deslumbrante. Arquitetura premiada, frontal Lagoa, apartamento
              em lamina!
              4 suítes , 4 vagas, lavabo, dependência de empregada completa, piscina, churrasqueira, forno a lenha e
              fechamento em cortina de vidro.

              <!-- Borges 3647 Lagoa traz para você luxo e elegância em um dos bairros mais nobres do Rio de Janeiro. Borges 3647 Performance exibe sofisticação com toda originalidade localizado na Lagoa.
          <br /><br />
          Um dos bairros mas exclusivos da Zona Sul do Rio de Janeiro onde viver bem é viver com tranquilidade e comodidade no seu dia-a-dia com a natureza exuberante que cerca o Borges 3647 Lagoa. -->
            </p>
            <div class="fichatec">
              <strong>DISPONIBILIDADE</strong>
              <br />
              Apto 101 com 315m²<br />
              4 suítes<br />
              4 vagas<br />
              churrasqueira e forno a Lenha
            </div>
            <a href="#header" class="queromaisinfo scroll">Quero mais informações</a>
          </div>

          <div class="col-md-7">

            <div class="foto01" style="background-image: url(style/images/foto_lagoa_02.jpg);"></div>
            <div class="foto02" style="background-image: url(style/images/foto_lagoa_03.jpg);"></div>
            <div class="foto03" style="background-image: url(style/images/foto_lagoa_04.jpg);"></div>
          </div>


        </div>
        <!--ROW-->
      </div>


    </div>
    <!--CONTAINER -->

  </div><!-- BOX CONT -->



  <!-- 
    BOX DE TEASER
    <div class="fotoprincipal princ2">
    <div class="row">
      <div class="col-md-12">
        <a href="#header" class="scroll">
          <img class="foto" src="style/images/img_info_lagoa.jpg" />
        </a>
        <span>Rua Lineu de Paula Machado, 708 - Lagoa - Breve Lançamento</span> 
      </div>
    </div>
  </div> -->



  <script src="style/lightbox/js/lightbox-plus-jquery.min.js"></script>

  <!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script> -->

  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

  <script>
  window.jQuery || document.write('<script src="assets/js/vendor/jquery.slim.min.js"><\/script>')
  </script>
  <script src="assets/dist/js/bootstrap.bundle.min.js"></script>



  <!-- functions -->
  <script>
  $(document).ready(function() {
    $('.owl-carousel').owlCarousel({
      loop: true,
      margin: 10,
      responsiveClass: true,
      responsive: {
        0: {
          items: 1,
          nav: true
        },
        600: {
          items: 1,
          nav: false
        },
        1000: {
          items: 3,
          nav: true,
          loop: false,
          margin: 20
        }
      }
    })
  })
  </script>

  <script src="style/js/functions.js"></script>

  <script src="style/owlslider/owl.carousel.min.js"></script>





  <script>
  document.getElementById("open-anapro").addEventListener("click", abreAnaPro);

  function abreAnaPro() {
    let link =
      "https://online.crm.anapro.com.br/WebCRMService/Pages/chat/cliente/v2/ChatClienteEntrada.aspx?conta=zb3W3IC4wqI1&keyIntegradora=3FE49887-F31C-4E33-88AE-08047FE8ABDF&keyAgencia=c91ef847-c457-4086-8b51-ca4ca73e39f8&strDir=performance&campanha=75FtzJRnu4s1&canal=WsE_0bWVr2o1&produto=tB_x1K5VpbA1&strmidia=LANDING+PAGE+-+ZONA+SUL&strpeca=&usuarioEmail=&strgrupopeca=&strcampanhapeca=&nome=&email=&telefoneDDD=&telefone=&strTexto=&keyexterno=&urlep=&urlcp=&urlca=&urlat=&strMostrarTopo=false&strAutoSubmit=true&strUsarDadosAnteriores=true&emailobrigatorio=true&telefoneobrigatorio=false&texto=";
    window.open(link, "Chat Vendas", "height=200,width=200");
  }

  $('.playvideo').click(function() {
    $(".playvideo").hide();
    $(".pausevideo").show();
  });

  $('.pausevideo').click(function() {
    $(".playvideo").show();
    $(".pausevideo").hide();
  });
  </script>

  <?php require_once 'integracao/integracao.php'; ?>

</body>

</html>