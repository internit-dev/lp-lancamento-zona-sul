<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Jekyll v4.1.1">
    <title>Imóveis na Zona Sul Rio de Janeiro para comprar ou investir Apartamentos de 2, 3 e 4 Quartos.</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/carousel/">

    <!-- Bootstrap core CSS -->
    <link href="assets/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- fontawesome -->
    <link href="fontawesome/css/all.css" rel="stylesheet">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    
    <!-- STYLE -->
    <!-- Owl Stylesheets -->
    <link rel="stylesheet" href="style/lightbox/css/lightbox.min.css">
    <link rel="stylesheet" href="assets/owlcarousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/owlcarousel/assets/owl.theme.default.min.css">

    <link rel="stylesheet" href="style/owlslider/owl.carousel.min.css" />
	<link rel='stylesheet' href='style/style.css' media='all' />
	<link rel='stylesheet' href='style/mobile.css' media='all' />

    <!-- javascript -->
    <!-- <script src="../assets/vendors/jquery.min.js"></script> -->
    <!-- <script src="../assets/owlcarousel/owl.carousel.js"></script> -->


<!-- Global site tag (gtag.js) - Google Ads: 638188706 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-638188706"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-B2T0HBKWRS');

  gtag('config', 'AW-638188706');
</script>	
	
	
    <!-- FONT -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">

	<?php 
$vUrl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; 

$nome     = '';
$email    = '';
$tel      = '';
$msg	  = '';	
if (isset($_POST['nome'])) {
$nome     = $_POST['nome'];
$email    = $_POST['email'];
$tel      = $_POST['telefone'];
$msg	  = $_POST['mensagem'];	
}

?>
	<script language="javascript" type="text/javascript">

                function checa(nform) {
					
					$('#enviar').hide();
		
                        if (formEmail.nome.value == "") {

                                alert("Preencha o Campo 'Nome'");

                                formEmail.nome.focus();

                                formEmail.nome.select();

								$('#enviar').show();
                                return false;

                        }

                        if (formEmail.email.value == "") {

                                alert("Preencha o Campo 'Email'");

                                formEmail.email.focus();

                                formEmail.email.select();

								$('#enviar').show();
                                return false;

                        }
						
						var oRegEmail = /^[a-z0-9\._\-]+\@[a-z0-9\._\-]+\.[a-z]{2,3}$/i;
						
						if (!oRegEmail.test(formEmail.email.value)) {
								alert('Por favor, confira seu endereço de e-mail.');
								formEmail.email.focus();
                                formEmail.email.select();
								$('#enviar').show();
								return false;
                  }

                        if (formEmail.telefone.value == "") {

                                alert("Preencha o Campo 'Telefone'");

                                formEmail.telefone.focus();

                                formEmail.telefone.select();

								$('#enviar').show();
                                return false;

                        }	
						
						var str = new String( formEmail.telefone.value );
                        if (str.length  < 14) {
                                alert("Preencha o Campo 'Telefone' completo");
                                formEmail.telefone.focus();
                                formEmail.telefone.select();
								$('#enviar').show();
                                return false;
                        }							
						
                        if (formEmail.empreendimento.value == "0") {

                                alert("Selecione o 'Empreendimento'");

                                formEmail.empreendimento.focus();

                                formEmail.empreendimento.select();

								$('#enviar').show();
                                return false;

                        }						

						$('#enviar').show(); 				
  formEmail.action='obrigado2.php';
  formEmail.method='post';
  formEmail.submit();
						 return true;

                }

				
	
function mask(o, f) {
  setTimeout(function() {
    var v = mphone(o.value);
    if (v != o.value) {
      o.value = v;
    }
  }, 1);
}

function mphone(v) {
  var r = v.replace(/\D/g, "");
  r = r.replace(/^0/, "");
  if (r.length > 10) {
    r = r.replace(/^(\d\d)(\d{5})(\d{4}).*/, "($1) $2-$3");
  } else if (r.length > 5) {
    r = r.replace(/^(\d\d)(\d{4})(\d{0,4}).*/, "($1) $2-$3");
  } else if (r.length > 2) {
    r = r.replace(/^(\d\d)(\d{0,5})/, "($1) $2");
  } else {
    r = r.replace(/^(\d*)/, "($1");
  }
  return r;
}

				
</script>
</head>
<body>


  <div class="menulateral">
  <div class="box email">
    <a class="scroll" href="#header">
      <i class="fas fa-envelope"></i>
      <span>Informações de venda</span>
    </a>
  </div>
  <div class="box chat">
    <a href="https://online.crm.anapro.com.br/WebCRMService/Pages/chat/cliente/v2/ChatClienteEntrada.aspx?conta=zb3W3IC4wqI1&keyIntegradora=3FE49887-F31C-4E33-88AE-08047FE8ABDF&keyAgencia=c91ef847-c457-4086-8b51-ca4ca73e39f8&strDir=performance&campanha=75FtzJRnu4s1&canal=WsE_0bWVr2o1&produto=tB_x1K5VpbA1&strmidia=LANDING+PAGE+-+ZONA+SUL&strpeca=&usuarioEmail=&strgrupopeca=&strcampanhapeca=&nome=&email=&telefoneDDD=&telefone=&strTexto=&keyexterno=&urlep=&urlcp=&urlca=&urlat=&strMostrarTopo=false&strAutoSubmit=true&strUsarDadosAnteriores=true&emailobrigatorio=true&telefoneobrigatorio=false&texto=" target="_blank">
      <i class="far fa-comments"></i>
      <span>Chat Vendas</span>
    </a>
  </div>
  <div class="box phone">
    <a href="https://api.whatsapp.com/send?phone=5521971227905&text=Ol%C3%A1%2C%20Gostaria%20de%20saber%20mais%20sobre%20os%20im%C3%B3veis%20da%20Zona%20Sul." target="_blank">
      <img src="style/images/icon_whatsapp.png" class="icon_whatsapp" />
      <span>Whatsapp</span>
    </a>
  </div>
</div>


<div class="header" id="header">
  <div class="container">
    
    <div class="row">
      <div class="col-md-12">
        <h1>Morar na Zona Sul.</h1>
        <h2>As melhores opções para morar ou investir em imóveis.</h2>
      </div>
    </div><!--ROW-->

    <div class="boxinfo">
      <div class="row">
        <div class="col-md-6">
          
          <div class="escolha">
            <h1>Escolha onde quer morar:</h1>
            <a class="scroll" href="#botafogo">Botafogo</a>
            <a class="scroll" href="#lagoa">Lagoa</a>
            <a class="scroll" href="#gavea">Gávea</a>
          </div>

        </div>
        <div class="col-md-6">
              
          <div class="contato" id="contato">
            <div class="container">
                <div class="form">
                    <h1>Quero mais Informações</h1>
                    <form name="formEmail" action="" method="post" >  
                        <input type="hidden" name="vUrl" id="vUrl" value="<?php echo $vUrl; ?>">

                        <input name="nome" class="inp email" type="text" id="nome" placeholder="Nome" value="<?php echo $nome; ?>" />
                        <input name="email" class="inp email" type="text" id="email" placeholder="E-mail" value="<?php echo $email; ?>"/>
                        <input name="telefone" class="inp email" type="text" class="tel" id="telefone" placeholder="Telefone"  MaxLength="15" onKeyDown="mask(this, mphone);" onKeyPress="mask(this, mphone);" onKeyUp="mask(this, mphone);" value="<?php echo $tel; ?>" />
						<select name="empreendimento" class="inp email" id="empreendimento" >
							<option value="0">Empreendimento</option>
							<option value="Jardim Botafogo">Jardim Botafogo</option>
							<option value="Conde de Iraja">Conde de Irajá</option>
							<option value="Borges 3647">Borges 3647</option>
							<option value="Lineu de Paula Machado">Lineu de Paula Machado</option>
							<option value="Contemporaneo Gavea">Contemporâneo Gávea</option>
						</select>
                        <textarea name="mensagem"  id="mensagem" rows="4" cols="50" placeholder="Mensagem"><?php echo $msg; ?></textarea>

                        <input type="submit" name="enviar" id="enviar" class="bt" value="Enviar "  onclick="javascript:return checa(this);"  />
                  </form>
                </div>
            </div>
        </div><!-- CONTATO -->

        <br class="clear" />

        </div>
      </div><!-- ROW -->
    </div><!--BOX INFO -->

  </div><!--CONTAINER -->

</div><!--HEADER-->


<div class="boxcont" id="botafogo">
 
  <div class="container">
    
    <div class="row">
      <div class="col-md-12">
        <h1 class="titulo">
          <span></span><span></span><span></span><span></span>
          Botafogo
          <span></span><span></span><span></span><span></span>
        </h1>
      </div>
    </div><!--ROW-->

    <div class="fotoprincipal">
    <div class="row">
      <div class="col-md-12">
        <img class="foto" src="style/images/foto_botafogo_01.jpg" />
        <span>Jardim Botafogo - 2, 3 e 4 Quartos</span> 
      </div>
    </div><!--ROW-->
  </div>

    <div class="infopredio">
    <div class="row">
      <div class="col-md-5">
        <p>
          O condomínio Jardim Botafogo une natureza, conforto e sofisticação em uma área de mais de 8 mil m² com toda a estrutura que a sua família precisa! Você merece viver em um dos melhores lançamentos da Zona Sul! Saiba mais sobre essa oportunidade incrível.

          <!-- O condomínio com a maior estrutura de lazer de Botafogo, que conduz para o bairro um empreendimento com design contemporâneo, que possuirá entretenimentos individualizados, segurança, sustentabilidade e tecnologia.
          <br /><br />
          Exclusivos apartamentos de 2, 3 e 4 quartos, de 76m² até 200m², que encantam pela sua sofisticação e vistas únicas! -->
        </p>
        <div class="fichatec">
          <strong>FICHA TÉCNICA</strong>
          <br />
          13 Unidades de 2qts<br />
          17 Unidades de 3qts
        </div>
        <a href="#header" class="queromaisinfo scroll">Quero mais informações</a>
      </div>

      <div class="col-md-7">

        <div class="foto01" style="background-image: url(style/images/foto_botafogo_02.jpg);" ></div>
        <div class="foto02" style="background-image: url(style/images/foto_botafogo_03.jpg);" ></div>
        <div class="foto03" style="background-image: url(style/images/foto_botafogo_04.jpg);" ></div>
      </div>


    </div><!--ROW-->
  </div><!--infopredio-->


  <div class="fotoprincipal princ2">
    <div class="row">
      <div class="col-md-12">
        <a href="#header" class="scroll">
          <img class="foto" src="style/images/img_info.jpg" />
        </a>
        <span>Lançamento Rua Conde de Irajá - Em Breve</span> 
      </div>
    </div><!--ROW-->
  </div>

  </div><!--CONTAINER -->

</div><!-- BOX CONT -->


<div class="boxcont boxcont_branco" id="lagoa">
 
  <div class="container">
    
    <div class="row">
      <div class="col-md-12">
        <h1 class="titulo">
          <span></span><span></span><span></span><span></span>
          Lagoa
          <span></span><span></span><span></span><span></span>
        </h1>
      </div>
    </div><!--ROW-->

    <div class="fotoprincipal">
    <div class="row">
      <div class="col-md-12">
        <img class="foto" src="style/images/foto_lagoa_01.jpg" />
        <span>Borges 3647</span> 
      </div>
    </div><!--ROW-->
  </div>

    <div class="infopredio">
    <div class="row">
      <div class="col-md-5">
        <p>
          Localizado na região da Lagoa, no Rio de Janeiro o projeto teve como premissa a valorização e integração com a paisagem marcante, através da presença de linhas que percorrem toda a fachada.
          <br /><br />
Um empreendimento único e uma localização deslumbrante.  Arquitetura premiada, frontal Lagoa, apartamento em lamina!
4 suítes , 4 vagas, lavabo, dependência de empregada completa, piscina, churrasqueira, forno a lenha e fechamento em cortina de vidro.

          <!-- Borges 3647 Lagoa traz para você luxo e elegância em um dos bairros mais nobres do Rio de Janeiro. Borges 3647 Performance exibe sofisticação com toda originalidade localizado na Lagoa.
          <br /><br />
          Um dos bairros mas exclusivos da Zona Sul do Rio de Janeiro onde viver bem é viver com tranquilidade e comodidade no seu dia-a-dia com a natureza exuberante que cerca o Borges 3647 Lagoa. -->
        </p>
        <div class="fichatec">
          <strong>FICHA TÉCNICA</strong>
          <br />
          Apto 101 com 315m²<br />
          4 suítes<br />
          4 vagas<br />
          churrasqueira e forno a Lenha 
        </div>
        <a href="#header" class="queromaisinfo scroll">Quero mais informações</a>
      </div>

      <div class="col-md-7">

        <div class="foto01" style="background-image: url(style/images/foto_lagoa_02.jpg);" ></div>
        <div class="foto02" style="background-image: url(style/images/foto_lagoa_03.jpg);" ></div>
        <div class="foto03" style="background-image: url(style/images/foto_lagoa_04.jpg);" ></div>
      </div>


    </div><!--ROW-->
  </div><!--infopredio-->


  <div class="fotoprincipal princ2">
    <div class="row">
      <div class="col-md-12">
        <a href="#header" class="scroll">
          <img class="foto" src="style/images/img_info_lagoa.jpg" />
        </a>
        <span>Rua Lineu de Paula Machado, 708 - Lagoa - Breve Lançamento</span> 
      </div>
    </div><!--ROW-->
  </div>


  </div><!--CONTAINER -->

</div><!-- BOX CONT -->



<div class="boxcont" id="gavea">
 
  <div class="container">
    
    <div class="row">
      <div class="col-md-12">
        <h1 class="titulo">
          <span></span><span></span><span></span><span></span>
          Gávea
          <span></span><span></span><span></span><span></span>
        </h1>
      </div>
    </div><!--ROW-->

    <div class="fotoprincipal">
    <div class="row">
      <div class="col-md-12">
        <img class="foto" src="style/images/foto_gavea_01.jpg" />
        <span>Contemporâneo Gávea</span> 
      </div>
    </div><!--ROW-->
  </div>

    <div class="infopredio">
    <div class="row">
      <div class="col-md-5">
        <p>
          A combinação perfeita entre arquitetura, inteligência e bem-estar.
Esse projeto foi pensado para você que busca um residencial que se adapte à sua forma de viver.
Arquitetura, inteligência e sofisticação, isso em um só lugar.
Varanda gourmet, integrada à sala, com churrasqueira a gás.
Iluminação natural e circulação de ar. Fechadura biométrica. Tomadas USB na sala e quartos. Locker para todas as unidades

          <!-- O condomínio com a maior estrutura de lazer de Botafogo, que conduz para o bairro um empreendimento com design contemporâneo, que possuirá entretenimentos individualizados, segurança, sustentabilidade e tecnologia.
          <br /><br />
          Exclusivos apartamentos de 2, 3 e 4 quartos, de 76m² até 200m², que encantam pela sua sofisticação e vistas únicas! -->
        </p>
        <div class="fichatec">
          <strong>FICHA TÉCNICA</strong>
          <br />
          Unidade 304 com 88m²<br />
          2qts<br />
          1 suíte<br />
          1 vaga <br />
        </div>
        <a href="#header" class="queromaisinfo scroll">Quero mais informações</a>
      </div>

      <div class="col-md-7">

        <div class="foto01" style="background-image: url(style/images/foto_gavea_02.jpg);" ></div>
        <div class="foto02" style="background-image: url(style/images/foto_gavea_03.jpg);" ></div>
        <div class="foto03" style="background-image: url(style/images/foto_gavea_04.jpg);" ></div>
      </div>


    </div><!--ROW-->
  </div><!--infopredio-->


  </div><!--CONTAINER -->

</div><!-- BOX CONT -->



    



<script src="style/lightbox/js/lightbox-plus-jquery.min.js"></script>

<!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script> -->

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<script>window.jQuery || document.write('<script src="assets/js/vendor/jquery.slim.min.js"><\/script>')</script>
<script src="assets/dist/js/bootstrap.bundle.min.js"></script>



<!-- functions -->
        <script>
            $(document).ready(function() {
              $('.owl-carousel').owlCarousel({
                loop: true,
                margin: 10,
                responsiveClass: true,
                responsive: {
                  0: {
                    items: 1,
                    nav: true
                  },
                  600: {
                    items: 1,
                    nav: false
                  },
                  1000: {
                    items: 3,
                    nav: true,
                    loop: false,
                    margin: 20
                  }
                }
              })
            })
          </script>

<script src="style/js/functions.js"></script>

<script src="style/owlslider/owl.carousel.min.js"></script>

<script>
              $('.playvideo').click(function(){
                $(".playvideo").hide();
                $(".pausevideo").show();
              });
              
              $('.pausevideo').click(function(){
                $(".playvideo").show();
                $(".pausevideo").hide();
              });
            </script>


</body>
</html>
